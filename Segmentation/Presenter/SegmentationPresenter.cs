﻿using Segmentation.Model.Objects;
using Segmentation.Model.Processors;
using System;
using System.Collections.Generic;

namespace Segmentation.Presenter
{
    /// <summary>
    /// Presenter
    /// </summary>
    public class SteganographyPresenter
    {
        /// <summary> Data processor </summary>
        private Processor processor;
        /// <summary> View </summary>
        private SegmentationView view;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"> Data repository </param>
        /// <param name="processor"> Data processor </param>
        /// <param name="view"> View </param>
        public SteganographyPresenter(Processor processor, SegmentationView view)
        {
            this.processor = processor;
            this.view = view;
            view.Presenter = this;
        }

        /// <summary>
        /// Get currently edited data file
        /// </summary>
        /// <returns> DataFile that is current input </returns>
        internal DataFile GetCurrentDataFile()
        {
            return processor.DataFile;
        }

        /// <summary>
        /// Get currently edited data file
        /// </summary>
        /// <returns> DataFile that is current input </returns>
        internal DataFile GetCurrentEditedFile()
        {
            return processor.EditedFile;
        }

        /// <summary>
        /// Set current input data file
        /// </summary>
        /// <param name="dt"> Input data file </param>
        internal void SetCurrentDataFile(DataFile dt)
        {
            processor.ResetHistory();
            processor.DataFile = dt;
            processor.EditedFile = dt;
        }

        /// <summary>
        /// Set one threshold that divides picture into two sections "background" and "object"
        /// Pixels with brightness values greater than threshold set as white
        /// Pixels with brightness values greater than threshold set as black or if keep is true then keeps color of object
        /// </summary>
        /// <param name="value"> Value of threshold </param>
        /// <param name="keep"> Keep object colors or not </param>
        /// <returns></returns>
        internal bool SetThreshold(string value, bool keep)
        {
            if (value == null || value.Trim().Length == 0) 
                return false;

            // parse
            string[] divided = value.Split(';');
            int[] v = new int[divided.Length];
            for (int i = 0; i < divided.Length; i++)
            {
                if (!int.TryParse(divided[i].Trim(), out v[i]))
                    return false;

                // next threshold is smaller than or same as previous
                if (v[i] < 0  || (i > 0 && v[i] <= v[i - 1]))
                    return false;
            }
            
            if (processor.DataFile == null)
                return false;

            string res = processor.EditImage(1, keep, threshold: v);

            return res.Length != 0;  //Threshold(v, keep);
        }

        /// <summary>
        /// Save output image
        /// </summary>
        /// <param name="name"> Name of output </param>
        /// <returns> Name of saved output </returns>
        internal string SaveImage(string name)
        {
            if (name == null || name.Trim().Length == 0)
                name = null;
            // escape \ and /
            else
            {
                name = name.Replace('/', '_');
                name = name.Replace('\\', '_');
            }

            string res = processor.SaveCurrentEdit(name);
            return res;
        }

        /// <summary>
        /// Save output file with informations about input
        /// </summary>
        /// <param name="name"> Name of output </param>
        /// <returns> Name of saved output </returns>
        internal string SaveTextInfo(string name)
        {
            if (name == null || name.Trim().Length == 0)
                name = null;
            // escape \ and /
            else
            {
                name = name.Replace('/', '_');
                name = name.Replace('\\', '_');
            }

            string res = processor.SaveCurrentInfo(name);
            return res;
        }

        /// <summary>
        /// Edit histogram
        /// Value:
        ///     - -1 = reset histogram
        ///     -  0 = Laplace
        ///     -  1 = Roberts-cross
        ///     -  2 = DIF
        /// </summary>
        /// <param name="value"> Type of edit - value of -1 or between 0 and 2 </param>
        /// <param name="edgeCase"> True if high angularity, false if low </param>
        /// <returns> True if successful, false if not </returns>
        internal bool EditHistogram(int value, bool edgeCase)
        {
            if (processor.DataFile == null)
                return false;

            // reset of histogram
            if (value == -1)
            {
                processor.EditHistogramOfCurrentDataFile(value, false);
                return true;
            }

            // check if value is allowed
            if (value < 0 || value > 4)
                return false;

            processor.EditHistogramOfCurrentDataFile(value, edgeCase);
            return true;
        }

        /// <summary>
        /// Undo
        /// </summary>
        internal void Undo()
        {
            processor.Undo();
        }

        /// <summary>
        /// Create a new output image using probability input
        /// </summary>
        /// <param name="value"> String with probabilities </param>
        /// <param name="keep"> Keep original image colors (true) or not (false) - if true only first probability is taken into account </param>
        /// <returns> String with used thresholds </returns>
        internal string SetProbability(string value, bool keep)
        {
            // parse
            string[] divided = value.Split(';');
            double[] v = new double[divided.Length];
            for (int i = 0; i < divided.Length; i++)
            {
                Console.WriteLine(divided[i]);

                if (!double.TryParse(divided[i].Trim(), out v[i]))
                    return "";

                // next probability is smaller than or same as previous
                if ( v[i] > 100 || v[i] < 0 || (i > 0 && v[i] <= v[i - 1]))
                    return "";


                v[i] /= 100.0;
            }

            return processor.EditImage(2, keep, perc: v); // processor.ObjectProbability(v, keep);
        }

        /// <summary>
        /// Create a new output image using histogram entropy method
        /// </summary>
        /// <param name="alphaStep"> Step of alpha used while optimizing </param>
        /// <param name="keep"> Keep original image colors (true) or not (false) - if true only first probability is taken into account </param>
        /// <returns> String with used threshold </returns>
        internal string HistogramEntropy(double alphaStep, bool keep)
        {
            if (alphaStep > 1 || processor.DataFile == null)
                return "";

            return processor.EditImage(5, keep, perc: new double[] { alphaStep }); 
        }

        /// <summary>
        /// Create a new output image using minimum error method
        /// </summary>
        /// <param name="keep"> Keep original image colors (true) or not (false) - if true only first probability is taken into account </param>
        /// <returns> String with used threshold </returns>
        internal string MinimumError(bool keep)
        {
            if (processor.DataFile == null)
                return "";

            return processor.EditImage(6, keep); 
        }

        /// <summary>
        /// Call OTSU calculation
        /// </summary>
        /// <param name="keep"> Keep colours of object </param>
        /// <returns> Empty string if unsuccessfu, otherwise string containing found threshold </returns>
        internal string OTSU(bool keep)
        {
            if (processor.DataFile == null)
                return "";

            string res = processor.EditImage(3, keep);
            return res;
        }

        /// <summary>
        /// Call ROTSU calculation
        /// </summary>
        /// <param name="txt"> Limits to ROTSU stopping conditions </param>
        /// <param name="keep"> Keep colours of object </param>
        /// <returns> Empty string if unsuccessfu, otherwise string containing found threshold </returns>
        internal string ROTSU(string txt, bool keep, bool rmBg)
        {
            string[] input = txt.Split(';');
            int winSize = 0;

            if (processor.DataFile == null)
                return "";

            int min, max;
            if ((!rmBg && input.Length != 2) || (rmBg && input.Length != 3))
            {
                min = 70;
                max = 204;
            }
            else
            {
                bool res1 = int.TryParse(input[0], out min);
                bool res2 = int.TryParse(input[1], out max);
                
                if (!res1 || !res2)
                    return "";
            }

            if (rmBg && (input.Length == 3 || input.Length == 1))
            {
                bool res1;
                if (input.Length == 1)
                    res1 = int.TryParse(input[0], out winSize);
                else
                    res1 = int.TryParse(input[2], out winSize);

                if (!res1)
                    return "";

            }
            else if (rmBg)
                return "";

            if (min < 0 || max > 1001 || min > max)
                return "";

            string res = processor.EditImage(9, keep, minLim: min, maxLim: max, rmBg: rmBg, window: winSize);
            return res;
        }

        /// <summary>
        /// Get coordinates of full positions in matrix
        /// </summary>
        /// <returns> Array with arrays of x and y values </returns>
        internal double[][] GetCurrentMatrixData()
        {
            DataFile dt = processor.DataFile;

            if (dt == null)
                return null;

            int[,] matrix = dt.Matrix;

            List<double> xVals = new List<double>();
            List<double> yVals = new List<double>();

            double step = (dt.MinMaxInMatrix[1] - dt.MinMaxInMatrix[0]) / 1000;
            for (int x = 0; x < matrix.GetLength(0); x++)
            {
                for (int y = 0; y < matrix.GetLength(1); y++)
                {
                    if (matrix[x,y] > 0)
                    {
                        xVals.Add(x);
                        yVals.Add(y * step + dt.MinMaxInMatrix[0]);
                    }
                }
            }
            double[][] data = new double[2][];
            data[0] = xVals.ToArray();
            data[1] = yVals.ToArray();

            return data;
        }

        /// <summary>
        /// Call 2D OTSU calculation
        /// </summary>
        /// <param name="keep"> Keep colours of object </param>
        /// <returns> Empty string if unsuccessfu, otherwise string containing found threshold </returns>
        internal string OTSU2D(bool keep)
        {
            if (processor.DataFile == null)
                return "";

            string res = processor.EditImage(4, keep);
            return res; 
        }

        internal string Sauvola(string txt, int type, bool keep)
        {
            string[] input = txt.Split(';');

            if (processor.DataFile == null || input.Length != 2)
                return "";

            int win, step;
            bool res1 = int.TryParse(input[0], out win);
            bool res2 = int.TryParse(input[1], out step);

            if (!res1 || !res2 || win <= 1 || step <= 0)
                return "";

            string res = processor.EditImage(7, keep, window: win, step: step, subtype: type);
            return res;
        }

        internal string Cheriet(string tolerance, bool keep)
        {
            if (processor.DataFile == null)
                return "";

            float tol;
            bool res1 = float.TryParse(tolerance, out tol);

            if (!res1 || tol < 0 || tol > 100)
                return "";
            tol /= 100;

            string res = processor.EditImage(8, keep, tolerance: tol);
            return res;

            throw new NotImplementedException();
        }

    }
}