﻿using System.Collections.Generic;
using Segmentation.Model.Objects;
using Segmentation.Model.DataManip;
using System;

namespace Segmentation.Model.Processors
{
    /// <summary>
    /// Class for the handling of inputs. Decides which processors' methods to call.
    /// </summary>
    public class Processor
    {
        /// <summary> Histogram processor </summary>
        private HistogramProcessor hist;
        /// <summary> Image processor </summary>
        private ImageProcessor img;

        /// <summary> Current input file </summary>
        private DataFile dataFile;
        public DataFile DataFile { get => dataFile; set => dataFile = value; }

        /// <summary> History of input files </summary>
        private List<DataFile> history;

        /// <summary> Current output file </summary>
        private DataFile editedFile;
        public DataFile EditedFile { get => editedFile; set => editedFile = value; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Processor()
        {
            history = new List<DataFile>();
            hist = new HistogramProcessor();
            img = new ImageProcessor();
        }

        /// <summary>
        /// Save current output image
        /// </summary>
        /// <param name="name"> Name of saved image </param>
        /// <returns> Name of saved image </returns>
        internal string SaveCurrentEdit(string name)
        {
            return FileManip.WriteFile(EditedFile, name);
        }

        /// <summary>
        /// Reset history
        /// </summary>
        internal void ResetHistory()
        {
            history = new List<DataFile>();
        }

        /// <summary>
        /// Save info about currently edited file
        /// </summary>
        /// <param name="name"> Name of saved file </param>
        /// <returns> Name of saved file </returns>
        internal string SaveCurrentInfo(string name)
        {
            return FileManip.SaveInfoAboutFile(DataFile, EditedFile, name);
        }

        /// <summary>
        /// Add output image to history
        /// </summary>
        /// <param name="dataFile">DataFile to add to history</param>
        private void AddToHistory(DataFile dataFile)
        {
            history.Add(dataFile);
            if (history.Count > Config.HistoryDepth)
                history.RemoveAt(history.Count - 1);
        }

        /// <summary>
        /// Undo edit  to output image
        /// </summary>
        internal void Undo()
        {
            if (history.Count > 0)
            {
                editedFile = history[history.Count - 1];
                history.RemoveAt(history.Count - 1);
            }
        }

        /// <summary>
        /// Edit input image
        /// Type of edit:
        ///     - 1 = thresholding using manual threshold (parameter threshold needs to be set)
        ///     - 2 = thresholding using probability of object (parameter perc must be set
        ///     - 3 = thresholding using OTSU
        /// </summary>
        /// <param name="type"> Type of edit </param>
        /// <param name="keep"> Keep object colours </param>
        /// <param name="threshold"> Threshold </param>
        /// <param name="perc"> Probability of object within image <0, 1> </param>
        /// <returns> Threshold </returns>
        internal string EditImage(int type, bool keep, int[] threshold = null, double[] perc = null, int window = 10, int step = 1, int subtype = 1, double tolerance = 0.95, int minLim = 70, int maxLim = 204, bool rmBg = false) {
            DataFile res = null;
            if (type == 1)
            {
                if (threshold == null)
                    return "";

                res = img.Threshold(DataFile, threshold, keep);
                res.ExportInfo = "Used method: Manual thresholding \n";
            }
            // percentage known
            else if (type == 2)
            {
                if (perc == null)
                    return "";

                threshold = img.ObjectProbability(DataFile, perc);
                res = img.Threshold(DataFile, threshold, keep);

                res.ExportInfo = "Used method: Object percentage \n";
            }
            // OTSU
            else if (type == 3)
            {
                threshold = new int[] { img.OTSU(DataFile) };
                res = img.Threshold(DataFile, threshold, keep);

                res.ExportInfo = "Used method: OTSU \n";
            }
            // 2D OTSU
            else if (type == 4)
            {
                threshold = img.OTSU2D(DataFile);
                res = img.Threshold2D(dataFile, threshold);

                res.ExportInfo = "Used method: 2D OTSU \n";
            }

            // histogram entropy
            else if (type == 5)
            {
                threshold = new int[] { img.HistogramEntropy(DataFile, perc[0]) };
                res = img.Threshold(DataFile, threshold, keep);

                res.ExportInfo = "Used method: Histogram entropy \n";
            }

            // minimum error
            else if (type == 6)
            {
                threshold = new int[] { img.MinimumError(DataFile) };
                res = img.Threshold(DataFile, threshold, keep);

                res.ExportInfo = "Used method: Minimum error \n";
            }

            // niblack
            else if (type == 7)
            {
                threshold = new int[] { -1 };
                int[,] thresholds = img.Niblack(DataFile, window, step, subtype, keep);
                res = img.ThresholdLocal(DataFile, thresholds, keep);

                res.ExportInfo = "Used method: Niblack subtype ";
                switch (subtype)
                {
                    case 0: res.ExportInfo += "Sauvola \n";
                        break;
                    case 1: res.ExportInfo += "Niblack \n";
                        break;
                    case 2: res.ExportInfo += "Wolf \n";
                        break;
                }
                res.ExportInfo += "\tWindow size: " + window + "\n";
                res.ExportInfo += "\tStep size: " + step + "\n";

                res.ExportInfo += "Used threshold(s): \n";
                for (int i = 0; i < thresholds.GetLength(0); i++)
                {
                    for (int j = 0; j < thresholds.GetLength(1); j++)
                    {
                        res.ExportInfo += $"{thresholds[i,j]}, ";
                    }
                    res.ExportInfo += $"\n";
                }
            }

            // cheriet
            else if (type == 8)
            {
                threshold = new int[] { img.Cheriet(DataFile, tolerance) };
                if (threshold[0] == -1)
                    return "";

                res = img.Threshold(DataFile, threshold, keep);
                res.PreProcessFiles = img.History;

                res.ExportInfo = "Used method: Cheriet \n";
            }

            // ROTSU
            else if (type == 9)
            {
                threshold = new int[] { img.ROTSU(DataFile, rmBg, window, minLim, maxLim) };
                res = img.Threshold(DataFile, threshold, keep);
                res.PreProcessFiles = img.History;

                res.ExportInfo = "Used method: ROTSU \n";
                if (rmBg)
                    res.ExportInfo += "\tBackground removed\n\tWindow size: " + window + "\n";
                res.ExportInfo += "\tLimits: " + minLim + " and "+ maxLim + "\n";
            }

            else return "";

            // take note of last used threshold
            if (type != 7)
                res.ExportInfo += $"Used threshold(s): {string.Join("; ", threshold)} \n";

            // add file to history
            if (editedFile != null)
                AddToHistory(editedFile);
            else
                AddToHistory(dataFile);
            EditedFile = res;

            string thresh = string.Join(";", threshold);
            return thresh;
        }

        /// <summary>
        /// Edit histogram of input file
        /// Type:
        ///     - -1 = Revert to original
        ///     -  0 = Laplace
        ///     -  1 = Roberts-Cross
        ///     -  2 = DIF
        /// </summary>
        /// <param name="type"> Type of edit </param>
        /// <param name="edgeCase"> True if high angularity, false if not </param>
        /// <returns> True if successfull, false if not </returns>
        internal bool EditHistogramOfCurrentDataFile(int type, bool edgeCase)
        {
            string editName = "low angularity";
            if (edgeCase)
                editName = "high angularity";

            // revert
            if (type == -1)
            {
                dataFile.CalculateHistogram();
                dataFile.HistogramType = "original"; 
            }
            // Laplace
            else if (type == 0)
            {
                hist.EditHistogramLaplace(dataFile, edgeCase);
                dataFile.HistogramType = "laplace operator " + editName;
            }
            // Roberts-cross
            else if (type == 1)
            {
                hist.EditHistogramRoberts(dataFile, edgeCase);
                dataFile.HistogramType = "roberts-cross operator " + editName;
            }
            // DIF
            else if (type == 2)
            {
                hist.EditHistogramDIF(dataFile, edgeCase);
                dataFile.HistogramType = "DIF operator " + editName;
            }
            // Sobel
            else if (type == 3)
            {
                hist.EditHistogramDIF(dataFile, edgeCase);
                dataFile.HistogramType = "Sobel operator " + editName;
            }
            // ROB
            else if (type == 4)
            {
                hist.EditHistogramRobinson(dataFile, edgeCase);
                dataFile.HistogramType = "Robinson operator " + editName;
            }
            else return false;

            return true;
        }
    }
}



/*
    /// <summary>
    /// Edit input data based on threshold
    /// Pixels with brightness values greater than threshold set as white
    /// Pixels with brightness values greater than threshold set as black or if keep is true then keeps color of object
    /// </summary>
    /// <param name="threshold"></param>
    /// <param name="keep"></param>
    /// <returns></returns>
    internal DataFile Threshold(int threshold, bool keep)
    {
        // vzít, projít, porovnat s thresholdem, když keep tak zachovávam barvy, jenom ořezávam? když false tak b&w
        DataFile df = new DataFile();

        df.Name = dataFile.Name + "_out";
        df.Extension = dataFile.Extension;

        Bitmap bmp = dataFile.Image;
        Bitmap outBmp = new Bitmap(bmp);

        for (int h = 0; h < bmp.Height; h++)
        {
            for (int w = 0; w < bmp.Width; w++)
            {
                Color px = bmp.GetPixel(w, h);
                int q = (int)(px.GetBrightness() * 1000);

                if (!keep)
                {
                    if (q > threshold)
                        outBmp.SetPixel(w, h, Color.White);
                    else
                        outBmp.SetPixel(w, h, Color.Black);
                }
                else
                {
                    if (q > threshold)
                        outBmp.SetPixel(w, h, Color.White);
                    else
                        outBmp.SetPixel(w, h, px);
                }
            }
        }

        df.Image = outBmp;
        if (editedFile != null)
            AddToHistory(editedFile);
        else
            AddToHistory(dataFile);
        EditedFile = df;

        return df;
    }

    internal int ObjectProbability(double v, bool keep)
    {
        double sum = 0;
        int threshold = 0;
        double allSum = 0;
        for (int i = 0; i < dataFile.Histogram.Length; i++)
            allSum += dataFile.Histogram[i];

        for (int i = 0; i < dataFile.Histogram.Length; i++)
        {
            sum += dataFile.Histogram[i];
            double res = 1 / allSum * sum;
            threshold = i;

            if (Math.Abs(res - v) <= 0.001)
                break;
        }

        DataFile df = new DataFile();

        df.Name = dataFile.Name + "_out";
        df.Extension = dataFile.Extension;

        Bitmap bmp = dataFile.Image;
        Bitmap outBmp = new Bitmap(bmp);

        for (int h = 0; h < bmp.Height; h++)
        {
            for (int w = 0; w < bmp.Width; w++)
            {
                Color px = bmp.GetPixel(w, h);
                int q = (int)(px.GetBrightness() * 1000);

                if (!keep)
                {
                    if (q > threshold)
                        outBmp.SetPixel(w, h, Color.White);
                    else
                        outBmp.SetPixel(w, h, Color.Black);
                }
                else
                {
                    if (q > threshold)
                        outBmp.SetPixel(w, h, Color.White);
                    else
                        outBmp.SetPixel(w, h, px);
                }
            }
        }

        df.Image = outBmp;
        if (editedFile != null)
            AddToHistory(editedFile);
        else
            AddToHistory(dataFile);
        EditedFile = df;

        return threshold;
    }

    internal int OTSU()
    {
        double max = 0;
        int threshold = 0;
        for (int i = 0; i < dataFile.Histogram.Length; i++)
        {
            double ut = 0;
            double wk = 0;
            double uk = 0;

            for (int j = 0; j < dataFile.Histogram.Length; j++)
            {
                double pj = (dataFile.Histogram[j] / (dataFile.Image.Width * dataFile.Image.Height));
                ut += (j + 1) * pj;
                if (j <= i)
                {
                    wk += pj;
                    uk += (j + 1) * pj;
                }
            }

            double val = ((ut * wk - uk) * (ut * wk - uk)) / (wk * (1 - wk));
            if (val > max)
            {
                max = val;
                threshold = i;
            }
        }

        Threshold(threshold, false);
        return threshold;
    }
    */