﻿using Segmentation.Model.Objects;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Segmentation.Model.Processors
{
    /// <summary>
    /// Class manipulating thresholds
    /// </summary>
    class ImageProcessor
    {
        /// <summary> Last variance between classes </summary>
        double lastMaxVar;

        /// <summary> Number of black pixels in last thresholding </summary>
        int blackPixels;

        /// <summary> History of files created during thresholding </summary>
        List<DataFile> history;
        public List<DataFile> History { get => history; set => history = value; }

        /// <summary>
        /// Found slot into which value belongs
        /// </summary>
        /// <param name="thresh"> Thresholds </param>
        /// <param name="value"> Value </param>
        /// <returns></returns>
        private int SlotValue(int[] thresh, int value)
        {
            for (int i = 0; i < thresh.Length; i++)
            {
                if (value <= thresh[i])
                    return i;
            }

            return thresh.Length;
        }

        /// <summary>
        /// Edit input data based on threshold
        /// Pixels with brightness values greater than threshold set as white, pixels with lower set as shades of gray (depends on under which threshold they belong)
        /// If keep is true takes into account only the first threshold, and pixels that have lower brightness keep their color
        /// </summary>
        /// <param name="threshold"> Threshold </param>
        /// <param name="keep"> True if colours of object should be kept, false if not </param>
        /// <returns> New data file with edited bitmap </returns>
        internal DataFile Threshold(DataFile dataFile, int[] threshold, bool keep)
        {
            Color[] palette = new Color[threshold.Length + 1];
            palette[0] = Color.Black;
            palette[palette.Length - 1] = Color.White;

            if (palette.Length > 2)
            {
                int step = 255 / palette.Length - 2;
                for (int i = 1; i < palette.Length - 1; i++)
                    palette[i] = Color.FromArgb(step * i, step * i, step * i);
            }

            blackPixels = 0;
            DataFile df = new DataFile();

            df.Name = dataFile.Name + "_out";
            df.Extension = dataFile.Extension;

            Bitmap bmp = dataFile.Image;
            Bitmap outBmp = new Bitmap(bmp);

            // for all pixels
            for (int h = 0; h < bmp.Height; h++)
            {
                for (int w = 0; w < bmp.Width; w++)
                {
                    // get brightness
                    Color px = bmp.GetPixel(w, h);
                    int q = (int)(px.GetBrightness() * 1000);
                    int value = SlotValue(threshold, q);

                    if (value != threshold.Length)
                        blackPixels++;

                    // if keep colours
                    if (!keep)
                    {
                        outBmp.SetPixel(w, h, palette[value]);
                    }
                    // if output b&w
                    else
                    {
                        if (q > threshold[0])
                            outBmp.SetPixel(w, h, Color.White);
                        else
                            outBmp.SetPixel(w, h, px);
                    }
                }
            }

            df.Image = outBmp;
            return df;
        }

        /// <summary>
        /// Threshold image using local thresholds for each pixel
        /// Pixels with brightness values greater than threshold set as white, pixels with lower set as black or white
        /// If keep is true takes into account only the first threshold, and pixels that have lower brightness keep their color
        /// </summary>
        /// <param name="threshold"> Threshold </param>
        /// <param name="keep"> True if colours of object should be kept, false if not </param>
        /// <returns> New data file with edited bitmap </returns>
        internal DataFile ThresholdLocal(DataFile dataFile, int[,] thresholds, bool keep)
        {
            DataFile df = new DataFile();

            df.Name = dataFile.Name + "_out";
            df.Extension = dataFile.Extension;

            Bitmap bmp = dataFile.Image;
            Bitmap outBmp = new Bitmap(bmp);

            // for all pixels
            for (int h = 0; h < bmp.Height; h++)
            {
                for (int w = 0; w < bmp.Width; w++)
                {
                    int threshold = thresholds[w, h];

                    // get brightness
                    Color px = bmp.GetPixel(w, h);
                    int q = (int)(px.GetBrightness() * 1000);

                    // if keep colours
                    if (!keep)
                    {
                      // black if under, white if above
                        if (q > threshold)
                            outBmp.SetPixel(w, h, Color.White);
                        else
                            outBmp.SetPixel(w, h, Color.Black);
                    }
                    // if output b&w
                    else
                    {
                        if (q > threshold)
                            outBmp.SetPixel(w, h, Color.White);
                        else
                            outBmp.SetPixel(w, h, px);
                    }
                }
            }

            df.Image = outBmp;
            return df;
        }


        /// <summary>
        /// Find local thresholds using Niblack technique
        /// </summary>
        /// <param name="dataFile"></param>
        /// <param name="window"></param>
        /// <param name="step"></param>
        /// <param name="type"> 0 - Sauvola, 1 - Niblack, 2 - Wolf </param>
        /// <param name="keep"></param>
        /// <returns> threshold for each pixel </returns>        
        internal int[,] Niblack(DataFile dataFile, int window, int step, int type, bool keep)
        {
            Bitmap bmp = dataFile.Image;

            double minM = double.MaxValue, maxS = double.MinValue;
            if (type == 2)
            {
                for (int h = 0; h < bmp.Height; h+=step)
                {
                    for (int w = 0; w < bmp.Width; w+=step)
                    {
                        double m = 0;
                        int count = 0;
                        // compute in mean in window - size of window
                        for (int i = -window / 2; i < window / 2; i++)
                        {
                            for (int j = -window / 2; j < window / 2; j++)
                            {
                                Color px = bmp.GetPixel(Math.Min(Math.Max(w + i, 0), bmp.Width - 1), Math.Min(Math.Max(h + j, 0), bmp.Height - 1));
                                int q = (int)(px.GetBrightness() * 1000);
                                count++;
                                m += q;
                            }
                        }
                        m /= count;

                        if (m < minM)
                            minM = m;

                        // compute in stDev in window - size of window
                        double s = 0;
                        for (int i = -window / 2; i < window / 2; i++)
                        {
                            for (int j = -window / 2; j < window / 2; j++)
                            {
                                Color px = bmp.GetPixel(Math.Min(Math.Max(w + i, 0), bmp.Width - 1), Math.Min(Math.Max(h + j, 0), bmp.Height - 1));
                                int q = (int)(px.GetBrightness() * 1000);
                                s += (q - m) * (q - m);
                            }
                        }
                        s /= count;
                        s = Math.Sqrt(s);

                        if (s > maxS)
                            maxS = s;
                    }
                }
            }

            // array with thresholds
            int[,] thresholds = new int[bmp.Width, bmp.Height];
            
            // for all pixels
            for (int h = 0; h < bmp.Height; h+=step)
            {
                for (int w = 0; w < bmp.Width; w+=step)
                {
                    double m = 0;
                    int count = 0;
                    // compute in mean in window
                    for (int i = -window/2; i < window/2; i++)
                    {
                        for (int  j = -window / 2; j < window / 2; j++)
                        {
                            Color px = bmp.GetPixel(Math.Min(Math.Max(w + i, 0), bmp.Width-1), Math.Min(Math.Max(h + j, 0), bmp.Height-1));
                            int q = (int)(px.GetBrightness() * 1000);
                            count++;
                            m += q;
                        }
                    }
                    m /= count;

                    // compute stDev in window
                    double s = 0;
                    for (int i = -window / 2; i < window / 2; i++)
                    {
                        for (int j = -window / 2; j < window / 2; j++)
                        {
                            Color px = bmp.GetPixel(Math.Min(Math.Max(w + i, 0), bmp.Width-1), Math.Min(Math.Max(h + j, 0), bmp.Height-1));
                            int q = (int)(px.GetBrightness() * 1000);
                            s += (q - m) * (q - m);
                        }
                    }
                    s /= count;
                    s = Math.Sqrt(s);

                    // get brightness
                    Color pxC = bmp.GetPixel(w, h);
                    int qC = (int)(pxC.GetBrightness() * 1000);

                    // compute threshold - based on type
                    int threshold = 0;
                    if (type == 0)
                    {
                        threshold = (int)(m - 0.2 * s);
                    }
                    else if (type == 1)
                    {
                        threshold = (int)(m * (1 + 0.5 * (s / 128 - 1)));
                    }
                    else if (type == 2)
                    {
                        threshold = (int)(m - 0.5 * (1 - s / maxS) * (m - minM));
                    }

                    thresholds[w, h] = threshold;
                }
            }

            // for all pixels - averagning what was not computed directly
            for (int h = 0; h < bmp.Height; h++)
            {
                for (int w = 0; w < bmp.Width; w++)
                {
                    int wRes = w % step;
                    int hRes = h % step;
                    
                    if (wRes == 0 && hRes == 0)
                        continue;

                    // NEAREST NEIGHBOR
                    int[,] neighbors = new int[4,2];
                    // minus h, minus w -> left top
                    neighbors[0, 0] = (w - wRes);
                    neighbors[0, 1] = (h - hRes);

                    // plus h, minus w -> left bottom
                    neighbors[1, 0] = (w - wRes);
                    neighbors[1, 1] = (h + (step - hRes));

                    // minus h, plus w -> right top
                    neighbors[2, 0] = (w + (step - wRes));
                    neighbors[2, 1] = (h - hRes);

                    // plus h, plus w -> right bottom
                    neighbors[3, 0] = (w + (step - wRes));
                    neighbors[3, 1] = (h + (step - hRes));

                    // -> which one is the closest in manhattan distance
                    int minIndex = 0;
                    int min = int.MaxValue;
                    for (int i = 0; i < neighbors.GetLength(0); i++)
                    {
                        if (neighbors[i, 0] >= 0 && neighbors[i, 0] < bmp.Width && neighbors[i, 1] >= 0 && neighbors[i, 1] < bmp.Height) {
                            if ((Math.Abs(w - neighbors[i, 0]) + Math.Abs(h - neighbors[i, 1])) < min)
                            {
                                min = (Math.Abs(w - neighbors[i, 0]) + Math.Abs(h - neighbors[i, 1]));
                                minIndex = i;
                            }
                        }
                    }

                    int nW = neighbors[minIndex, 0];
                    int nH = neighbors[minIndex, 1];

                    // assign the threshold of that pixel
                    thresholds[w, h] = thresholds[nW, nH];
                }
            }

            return thresholds;
        }

        /// <summary>
        /// Finds threshold based on the probability of object in image
        /// </summary>
        /// <param name="dataFile"> Data file </param>
        /// <param name="prob"> Probabilities between <0, 1> </param>
        /// <returns> Threshold </returns>
        internal int[] ObjectProbability(DataFile dataFile, double[] prob)
        {
            double sum = 0;
            int[] threshold = new int[prob.Length];
            double allSum = 0;

            // if 100% object
            if (prob[0] == 1) {
                threshold[0] = dataFile.Histogram.Length;
                return threshold;
            }

            for (int i = 0; i < dataFile.Histogram.Length; i++)
                allSum += dataFile.Histogram[i];

            int current = 0;
            for (int i = 0; i < dataFile.Histogram.Length; i++)
            {
                sum += dataFile.Histogram[i];
                double res = 1 / allSum * sum;

                // epsilon testing
                if ((prob[current] - res) <= 0.001)
                {
                    threshold[current] = i;
                    current++;
                    if (current >= prob.Length)
                        break;
                }
            }

            return threshold;
        }

        /// <summary>
        /// Finds threshold based on the entropy of the histogram
        /// </summary>
        /// <param name="dataFile"> Data file </param>
        /// <param name="alphaStep"> Step in which is changing the parameter alpha </param>
        /// <returns> Threshold </returns>
        internal int HistogramEntropy(DataFile dataFile, double alphaStep)
        {
            int threshold = 0;
            double allSum = 0;

            for (int i = 0; i < dataFile.Histogram.Length; i++)
                allSum += dataFile.Histogram[i];

            // compute entropy H
            double pi, H = 0, sumPi = 0;
            for (int i = 0; i < dataFile.Histogram.Length; i++)
            {
                pi = dataFile.Histogram[i] / allSum;

                if (pi == 0)
                    continue;

                H += pi * Math.Log(pi, 2);
                sumPi += pi;
            }
            H = -H;

            double F, currAlpha = 0.1, maxF = double.MinValue;
            while (currAlpha < 1)
            {
                // compute current k - while cycle until sum of probab * log(probab) < currAlpha * H
                int k = 0;
                double right = (-1) * currAlpha * H;
                double left = 0, maxK = double.MinValue, sumK = 0;
                
                for (int i = 0; i < dataFile.Histogram.Length; i++)
                {
                    pi = dataFile.Histogram[i] / allSum;
                    
                    if (pi != 0)
                        left += pi * Math.Log(pi, 2);

                    // both left and right are negative values
                    if (left <= right) 
                        break;

                    k = i;
                    sumK += pi;
                    if (pi > maxK)
                        maxK = pi;
                }

                // copute parametric function F(alpha)
                double maxK1 = double.MinValue;
                for (int i = k; i < dataFile.Histogram.Length; i++)
                {
                    pi = dataFile.Histogram[i] / allSum;
                    if (pi > maxK1)
                        maxK1 = pi;
                }
                F = (currAlpha * Math.Log(sumK, 2)) / Math.Log(maxK, 2) + ((1-currAlpha)* Math.Log(sumPi - sumK, 2)) / Math.Log(maxK1, 2);

                // save k and F(alpha) if its bigger than current saved values
                if (F >= maxF)
                {
                    maxF = F;
                    threshold = k;
                }

                currAlpha = currAlpha + alphaStep;
            }

            return threshold;
        }

        /// <summary>
        /// Finds threshold based on the minimum error method
        /// </summary>
        /// <param name="dataFile"> Data file </param>
        /// <returns> Threshold </returns>
        internal int MinimumError(DataFile dF)
        {
            int threshold = 0;
            double allSum = 0;

            DataFile dataFile = dF.DeepCopy();
            dataFile.Image = Average(dataFile.Image, 3);
            dataFile.CalculateHistogram();

            for (int i = 0; i < dataFile.Histogram.Length; i++)
                allSum += dataFile.Histogram[i];

            // finding min for all thresholds
            double minJ = double.MaxValue;
            for (int t = 0; t < dataFile.Histogram.Length; t++)
            {
                // compute probability of pixel in C0 and C1
                double prob0 = 0;
                double mu0 = 0;
                for (int i = 0; i < t; i++)
                {
                    prob0 += dataFile.Histogram[i] / allSum;
                    mu0 += i * (dataFile.Histogram[i] / allSum);
                }
                if (prob0 != 0)
                    mu0 /= prob0;

                double mu1 = 0;
                double prob1 = 0;
                for (int i = t; i < dataFile.Histogram.Length; i++)
                {
                    prob1 += dataFile.Histogram[i] / allSum;
                    mu1 += i * (dataFile.Histogram[i] / allSum);
                }
                if (prob1 != 0)
                    mu1 /= prob1;

                // compute std. deviation in C0 and C1
                double stdev0 = 0;
                for (int i = 0; i < t; i++)
                    stdev0 += (i - mu0) * (i - mu0) * (dataFile.Histogram[i] / allSum);
                if (prob0 != 0)
                    stdev0 /= prob0;

                double stdev1 = 0;
                for (int i = t; i < dataFile.Histogram.Length; i++)
                    stdev1 += (i - mu1) * (i - mu1) * (dataFile.Histogram[i] / allSum);
                if (prob1 != 0)
                    stdev1 /= prob1;

                // compute cost function
                double j1p0 =  prob0 * Math.Log(Math.Sqrt(stdev0), 2);
                double j1p1 =  prob1 * Math.Log(Math.Sqrt(stdev1), 2);
                double j2p0 = prob0 * Math.Log(prob0, 2);
                double j2p1 = prob1 * Math.Log(prob1, 2);

                double J1 = 2 * (j1p0 + j1p1);
                double J2 = 2 * (j2p0 + j2p1);
                double J = 1 + J1 - J2;

                if (J <= minJ && !double.IsNaN(J) && !double.IsInfinity(J))
                {
                    minJ = J;
                    threshold = t;
                }
            }
            
            return threshold;
        }

        /// <summary>
        /// Compute separability factor
        /// </summary>
        /// <param name="processing"> Current state of dataFile </param>
        /// <returns> Sep. factor </returns>
        private double ComputeSeparabilityFactor(DataFile processing)
        {
            double allSum = 0;

            for (int i = 0; i < processing.Histogram.Length; i++)
                allSum += processing.Histogram[i];

            double muT = 0;
            for (int i = 0; i < processing.Histogram.Length; i++)
            {
                double pi = processing.Histogram[i] / allSum;
                muT += i * pi;
            }

            double sT = 0;
            for (int i = 0; i < processing.Histogram.Length; i++)
            {
                double pi = processing.Histogram[i] / allSum;
                sT += (i - muT) * (i - muT) * pi;
            }
         
            double sepFac = lastMaxVar / sT;

            return sepFac;
        }

        /// <summary>
        /// Averages the input bitmap based on a 3x3 neighborhood
        /// </summary>
        /// <param name="image"> Input </param>
        /// <returns> Averaged bitmap </returns>
        public Bitmap Average(Bitmap image, int winSize)
        {
            Bitmap temp;

            Color c;
            temp = new Bitmap(image.Width, image.Height);
            for (int i = 0; i < image.Width; i++)
                for (int j = 0; j < image.Height; j++)
                {
                    int count = 0;
                    // colour sum
                    int[] sum = new int[] { 0, 0, 0 };
                    for (int x = i - winSize / 2; x <= i + winSize / 2; x++)
                        for (int y = j - winSize / 2; y <= j + winSize / 2; y++)
                            if (x >= 0 && x < image.Width && y >= 0 && y < image.Height)
                            {
                                c = image.GetPixel(x, y);
                                sum[0] += c.R;
                                sum[1] += c.G;
                                sum[2] += c.B;
                                count++;
                            }
                    // average each pixel
                    Color newColor = Color.FromArgb(sum[0] / count, sum[1] / count, sum[2] / count);
                    temp.SetPixel(i, j, newColor);
                }
            return temp;
        }

        /// <summary>
        /// Finds threshold using the Cheriet method
        /// </summary>
        /// <param name="dataFile"> Data file </param>
        /// <returns> Threshold </returns>
        internal int Cheriet(DataFile dataFile, double tolerance)
        {
            int threshold = 1001;
            double sepFac;

            History = new List<DataFile>();

            DataFile processing = dataFile.DeepCopy();

            // 3x3 avg filter on image -> histogram
            processing.Image = Average(processing.Image, 3);
            processing.CalculateHistogram();

            History.Add(processing.DeepCopy());

            while (true)
            {
                // 1. OTSU -> get the darkest component
                int currthreshold = OTSU(processing);

                // separability factor
                sepFac = ComputeSeparabilityFactor(processing);

                Console.WriteLine(currthreshold + " \t " + sepFac);

                if (double.IsNaN(sepFac))
                    return -1;

                if (sepFac > tolerance)
                {
                    threshold = currthreshold;
                    break;
                }

                // fill bg w/ white -> histogram
                // new image from threshold
                processing = Threshold(processing, new int[] { currthreshold }, true);
                processing.CalculateHistogram();

                History.Add(processing.DeepCopy());

                // OTSU -> C is darkest component
                // if separability < 0.95 konec
                // 2. OTSU -> get the darkest component
                threshold = OTSU(processing);

                // separability factor
                sepFac = ComputeSeparabilityFactor(processing);

                if (double.IsNaN(sepFac))
                    return -1;

                if (sepFac > tolerance) break;

                // fill bg w/ white -> histogram
                // new image from threshold
                processing = Threshold(processing, new int[] { threshold }, true);
                processing.CalculateHistogram(endThreshold: threshold);

                History.Add(processing.DeepCopy());
            }

            return threshold;
        }

        /// <summary>
        /// Finds threshold using the OTSU method
        /// </summary>
        /// <param name="dataFile"> Datafile </param>
        /// <returns> Threshold </returns>
        internal int OTSU(DataFile dataFile)
        {
            double max = 0;
            int threshold = 0;

            double sum = 0; // (dataFile.Image.Width * dataFile.Image.Height));
            for (int i = 0; i < dataFile.Histogram.Length; i++)
                sum += dataFile.Histogram[i];

            for (int i = 0; i < dataFile.Histogram.Length; i++)
            {
                double ut = 0;
                double wk = 0;
                double uk = 0;

                for (int j = 0; j < dataFile.Histogram.Length; j++)
                {
                    double pj = (dataFile.Histogram[j] / sum);
                    ut += (j + 1) * pj;
                    if (j <= i)
                    {
                        wk += pj;
                        uk += (j + 1) * pj;
                    }
                }

                double val = ((ut * wk - uk) * (ut * wk - uk)) / (wk * (1 - wk));
                if (val > max)
                {
                    max = val;
                    threshold = i;
                }
            }

            lastMaxVar = max;
            return threshold;
        }

        /// <summary>
        /// Finds threshold using the ROTSU method
        /// </summary>
        /// <param name="dataFile"> Datafile </param>
        /// <param name="removeBg"> Should the background be removed </param>
        /// <param name="minLim"> Minimum change of threshold limit </param>
        /// <param name="maxLim"> Maximum change of threshold limit</param>
        /// <returns> Threshold </returns>
        internal int ROTSU(DataFile dataFile, bool removeBg, int avgSize, int minLim = 70, int maxLim = 204)
        {
            DataFile currDT = dataFile.DeepCopy();
            DataFile currHistory;

            History = new List<DataFile>();

            bool first = true;
            int addedPx;
            int lastPixels = 0;
            int newPixels;
            int firstAddedPx = 0;
            int newT = 0, lastT = 1001;

            if (removeBg)
            {
                // average filter
                Bitmap averaged = Average(currDT.Image, avgSize);

                // remove background
                for (int w = 0; w < currDT.Image.Width; w++)
                    for (int h = 0; h < currDT.Image.Height; h++)
                    {
                        Color currC = currDT.Image.GetPixel(w,h);
                        Color avgC = averaged.GetPixel(w,h);
                        Color c = Color.FromArgb(255 - Math.Max(avgC.R - currC.R, 0),
                                                 255 - Math.Max(avgC.G - currC.G, 0),
                                                 255 - Math.Max(avgC.B - currC.B, 0));
                        currDT.Image.SetPixel(w, h, c);
                    }

                currHistory = new DataFile();
                currHistory.Image = (Bitmap) currDT.Image.Clone();
                History.Add(currHistory);
                currDT.CalculateHistogram();
            }

            while (true)
            {
                newT = OTSU(currDT);
                currHistory = Threshold(dataFile, new int[] { newT }, false);

                if (first)
                {
                    firstAddedPx = blackPixels;
                    lastPixels = firstAddedPx;
                    lastT = newT + 100;
                    first = !first;
                }

                newPixels = blackPixels;
                addedPx = newPixels - lastPixels;

                // added a big chunk of background
                //if (addedPx > firstAddedPx)
                //    break;

                Console.WriteLine("Difference " + (newT - lastT));

                // difference exceeds limits
                if (Math.Abs(newT - lastT) < minLim || Math.Abs(newT - lastT) > maxLim)
                    break;

                History.Add(currHistory);
                currDT.CalculateHistogram(startThreshold: newT);
                lastT = newT;
                lastPixels = newPixels;
            }

            return lastT;
        }

        /// <summary>
        /// Finds threshold using the two dimensional OTSU method, useful for noisy images
        /// </summary>
        /// <param name="dataFile"> Data file </param>
        /// <returns> Threshold </returns>
        internal int[] OTSU2D(DataFile dataFile)
        {

            // 2D matrix - [brigthness, average of neighborhood]
            double[,] vals = dataFile.Histogram2D;

            double[] mT = new double[2];
            for (int h = 0; h < vals.GetLength(0); h++)
            {
                for (int w = 0; w < vals.GetLength(1); w++)
                {
                    if (w == 0 || h == 0 || w == dataFile.Image.Width - 1 || h == dataFile.Image.Height - 1)
                        continue;

                    double pij = ((double)vals[w, h]) / (dataFile.Image.Width * dataFile.Image.Height);
                    mT[0] += w * pij;
                    mT[1] += h * pij;
                }
            }

            double max = double.MinValue;
            int k = 0, l = 0;
            double[] m0 = new double[2];
            double w0 = 0;
            for (int s = 0; s < vals.GetLength(0); s++)
            {
                for (int t = 0; t < vals.GetLength(1); t++)
                {
                    double pij = ((double)vals[s, t]) / (dataFile.Image.Width * dataFile.Image.Height);
                    w0 += pij;

                    if (w0 == 0)
                        continue;

                    m0[0] += s * pij;
                    m0[1] += t * pij;

                    // evaluate trace of matrix with current t and s
                    double trace = ( (mT[0] * w0 - m0[0]) * (mT[0] * w0 - m0[0]) + (mT[1] * w0 - m0[1]) * (mT[1] * w0 - m0[1])) / (w0* (1-w0));

                    // find max
                    if (trace > max)
                    {
                        max = trace;
                        k = s;
                        l = t;
                    }
                }
            }

            int[] threshold = new int[] { k, l };
            return threshold;
        }

        /// <summary>
        /// Filters image according to thresholds in a 2D histogram - objects are in the first quadrant
        /// </summary>
        /// <param name="dataFile"> Data file to edit </param>
        /// <param name="threshold"> Thresholds </param>
        /// <returns> Edited image </returns>
        public DataFile Threshold2D(DataFile dataFile, int[] threshold)
        {
            DataFile df = new DataFile();

            df.Name = dataFile.Name + "_out";
            df.Extension = dataFile.Extension;

            Bitmap bmp = dataFile.Image;
            Bitmap outBmp = new Bitmap(bmp);

            for (int h = 0; h < dataFile.Image.Height; h++)
            {
                for (int w = 0; w < dataFile.Image.Width; w++)
                {

                    if (w == 0 || h == 0 || w == dataFile.Image.Width - 1 || h == dataFile.Image.Height - 1)
                        continue;

                    Color pixel = dataFile.Image.GetPixel(w, h);

                    float n = dataFile.Image.GetPixel(w - 1, h - 1).GetBrightness();
                    n += dataFile.Image.GetPixel(w - 1, h).GetBrightness();
                    n += dataFile.Image.GetPixel(w - 1, h + 1).GetBrightness();

                    n += dataFile.Image.GetPixel(w, h - 1).GetBrightness();
                    n += dataFile.Image.GetPixel(w, h + 1).GetBrightness();

                    n += dataFile.Image.GetPixel(w + 1, h - 1).GetBrightness();
                    n += dataFile.Image.GetPixel(w + 1, h).GetBrightness();
                    n += dataFile.Image.GetPixel(w + 1, h + 1).GetBrightness();
                    float avgN = n / 8.0f;

                    // indices
                    int indX = (int)(pixel.GetBrightness() * 1000);
                    int indY = (int)(avgN * 1000);

                    if (indX < threshold[0] && indY < threshold[1])
                        outBmp.SetPixel(w, h, Color.Black);
                    else
                        outBmp.SetPixel(w, h, Color.White);
                }
            }

            df.Image = outBmp;
            return df;
        }
    }
}
