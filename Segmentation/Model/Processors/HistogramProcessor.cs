﻿using Segmentation.Model.Objects;
using System;
using System.Drawing;

namespace Segmentation.Model.Processors
{
    /// <summary>
    /// Class providing methods for editing histograms and angularity matrix
    /// </summary>
    class HistogramProcessor
    {
        // --------------------------------------------------------- MATRIX -------------------------------------------------------------

        /// <summary>
        /// Compute angularity matrix using Robinson opearator
        /// </summary>
        /// <param name="dt"> DataFile </param>
        /// <param name="maxValue"> Max value of Roberts-Cross operator </param>
        /// <param name="minValue"> Min value of Roberts-Cross operator </param>
        /// <param name="edgeCase"> True if high angularity, false if low </param>
        private void ComputeMatrixRobinson(DataFile dt, double maxValue, double minValue, bool edgeCase)
        {
            int[,] matrix = new int[1001, 1001];

            double step = (maxValue - minValue) / 1000; // maxValue / 1000;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w == dt.Image.Width - 1 || h == dt.Image.Height - 1))
                    {
                        weight = GetWeightRobinson(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        if (edgeCase)
                        {
                            int ang = (int)((weight - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                        else
                        {
                            int ang = (int)((1 / (1 + weight * weight) - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                    }

                }
            }

            dt.MinMaxInMatrix = new double[] { minValue, maxValue };
            dt.Matrix = matrix;
        }

        /// <summary>
        /// Compute angularity matrix using Roberts-Cross operator
        /// </summary>
        /// <param name="dt"> DataFile </param>
        /// <param name="maxValue"> Max value of Roberts-Cross operator </param>
        /// <param name="minValue"> Min value of Roberts-Cross operator </param>
        /// <param name="edgeCase"> True if high angularity, false if low </param>
        private void ComputeMatrixRobertsCross(DataFile dt, double maxValue, double minValue, bool edgeCase)
        {
            int[,] matrix = new int[1001, 1001];

            double step = (maxValue - minValue) / 1000; // maxValue / 1000;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(h == 0 || w == dt.Image.Width - 1))
                    {
                        weight = GetWeightRoberts(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        if (edgeCase)
                        {
                            int ang = (int)( (weight - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                        else
                        {
                            int ang = (int)( (1 / (1 + weight * weight) - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                        
                    }

                }
            }

            dt.MinMaxInMatrix = new double[] { minValue, maxValue };
            dt.Matrix = matrix;
        }

        /// <summary>
        /// Compute angularity matrix using Laplace operator
        /// </summary>
        /// <param name="dt"> DataFile </param>
        /// <param name="maxValue"> Max value of Laplace operator </param>
        /// <param name="minValue"> Min value of Laplace operator </param>
        /// <param name="edgeCase"> True if high angularity, false if low </param>
        private void ComputeMatrixLaplace(DataFile dt, double maxValue, double minValue, bool edgeCase)
        {
            int[,] matrix = new int[1001, 1001];

            double step = (maxValue - minValue) / 1000; // maxValue / 1000;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w == dt.Image.Width - 1 || h == dt.Image.Height - 1))
                    {
                        weight = GetWeightLaplace(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        if (edgeCase)
                        {
                            int ang = (int)((weight - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                        else
                        {
                            int ang = (int)( (1 / (1 + weight * weight) - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                    }

                }
            }

            dt.MinMaxInMatrix = new double[] { minValue, maxValue };
            dt.Matrix = matrix;
        }

        /// <summary>
        /// Compute angularity matrix using DIF operator
        /// </summary>
        /// <param name="dt"> DataFile </param>
        /// <param name="maxValue"> Max value of DIF operator </param>
        /// <param name="minValue"> Min value of DIF operator </param>
        /// <param name="edgeCase"> True if high angularity, false if low </param>
        private void ComputeMatrixDIF(DataFile dt, double maxValue, double minValue, bool edgeCase)
        {
            int[,] matrix = new int[1001, 1001];

            double step = (maxValue - minValue) / 1000; // maxValue / 1000;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w >= dt.Image.Width - 2 || h >= dt.Image.Height - 2))
                    {
                        weight = GetWeightDIF(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);
                            
                        if (edgeCase)
                        {
                            int ang = (int)((weight - minValue) / step);
                            matrix[quantized, ang]++;
                        }
                        else
                        {
                            int ang = (int)( (1 / (1 + weight * weight) - minValue) / step);
                            //int ang = (int)( weight * 1000);
                            matrix[quantized, ang]++;
                        }
                    }

                }
            }

            dt.MinMaxInMatrix = new double[] { minValue, maxValue };
            dt.Matrix = matrix;
        }

        // --------------------------------------------------------- OPERATORS -------------------------------------------------------------

        /// <summary>
        /// Get weight of pixel at [w,h] using the Robinson operator
        /// </summary>
        /// <param name="image"> Image </param>
        /// <param name="w"> Width </param>
        /// <param name="h"> Height </par m>
        /// <returns> Weight of pixel at [w, h] </returns>
        private float GetWeightRobinson(Bitmap image, int w, int h)
        { 
            float neighborhood1 = image.GetPixel(w - 1, h - 1).GetBrightness();
            neighborhood1 += image.GetPixel(w, h - 1).GetBrightness();
            neighborhood1 += image.GetPixel(w + 1, h - 1).GetBrightness();

            neighborhood1 += image.GetPixel(w - 1, h).GetBrightness();
            neighborhood1 -= 2 * image.GetPixel(w , h).GetBrightness();
            neighborhood1 += image.GetPixel(w + 1, h).GetBrightness();

            neighborhood1 -= image.GetPixel(w - 1, h + 1).GetBrightness();
            neighborhood1 -= image.GetPixel(w, h + 1).GetBrightness();
            neighborhood1 -= image.GetPixel(w + 1, h + 1).GetBrightness();

            float neighborhood2 = image.GetPixel(w - 1, h - 1).GetBrightness();
            neighborhood2 += image.GetPixel(w, h - 1).GetBrightness();
            neighborhood2 -= image.GetPixel(w + 1, h - 1).GetBrightness();

            neighborhood2 += image.GetPixel(w - 1, h).GetBrightness();
            neighborhood2 -= 2 * image.GetPixel(w, h).GetBrightness();
            neighborhood2 -= image.GetPixel(w + 1, h).GetBrightness();

            neighborhood2 += image.GetPixel(w - 1, h + 1).GetBrightness();
            neighborhood2 += image.GetPixel(w, h + 1).GetBrightness();
            neighborhood2 -= image.GetPixel(w + 1, h + 1).GetBrightness();

            float weight = Math.Max(Math.Abs(neighborhood1), Math.Abs(neighborhood2));
            return weight;
        }

        /// <summary>
        /// Get weight of pixel at [w,h] using the Sobel operator
        /// </summary>
        /// <param name="image"> Image </param>
        /// <param name="w"> Width </param>
        /// <param name="h"> Height </par m>
        /// <returns> Weight of pixel at [w, h] </returns>
        private float GetWeightSobel(Bitmap image, int w, int h)
        {
            float neighborhood1 = image.GetPixel(w - 1, h - 1).GetBrightness();
            neighborhood1 += 2 * image.GetPixel(w, h - 1).GetBrightness();
            neighborhood1 += image.GetPixel(w + 1, h - 1).GetBrightness();
            neighborhood1 -= image.GetPixel(w - 1, h + 1).GetBrightness();
            neighborhood1 -= 2 * image.GetPixel(w, h + 1).GetBrightness();
            neighborhood1 -= image.GetPixel(w + 1, h + 1).GetBrightness();

            float neighborhood2 = image.GetPixel(w - 1, h - 1).GetBrightness();
            neighborhood2 += 2 * image.GetPixel(w - 1, h).GetBrightness();
            neighborhood2 += image.GetPixel(w - 1, h + 1).GetBrightness();
            neighborhood2 -= image.GetPixel(w + 1, h - 1).GetBrightness();
            neighborhood2 -= 2 * image.GetPixel(w + 1, h).GetBrightness();
            neighborhood2 -= image.GetPixel(w + 1, h + 1).GetBrightness();

            float weight = Math.Max(Math.Abs(neighborhood1), Math.Abs(neighborhood2));
            return weight;
        }

        /// <summary>
        /// Get weight of pixel at [w,h] using the DIF operator
        /// </summary>
        /// <param name="image"> Image </param>
        /// <param name="w"> Width </param>
        /// <param name="h"> Height </par m>
        /// <returns> Weight of pixel at [w, h] </returns>
        private float GetWeightDIF(Bitmap image, int w, int h)
        {
            Color pixel = image.GetPixel(w, h);

            float s1 = pixel.GetBrightness();
            s1 += image.GetPixel(w, h - 1).GetBrightness();
            s1 += image.GetPixel(w + 1, h - 1).GetBrightness();
            s1 += image.GetPixel(w + 1, h).GetBrightness();
            s1 -= image.GetPixel(w, h + 1).GetBrightness();
            s1 -= image.GetPixel(w, h + 2).GetBrightness();
            s1 -= image.GetPixel(w + 1, h + 1).GetBrightness();
            s1 -= image.GetPixel(w + 1, h + 2).GetBrightness();

            float s2 = pixel.GetBrightness();
            s2 += image.GetPixel(w - 1, h).GetBrightness();
            s2 += image.GetPixel(w - 1, h + 1).GetBrightness();
            s2 += image.GetPixel(w, h + 1).GetBrightness();
            s2 -= image.GetPixel(w + 1, h).GetBrightness();
            s2 -= image.GetPixel(w + 2, h).GetBrightness();
            s2 -= image.GetPixel(w + 1, h + 1).GetBrightness();
            s2 -= image.GetPixel(w + 2, h + 1).GetBrightness();

            float weight = Math.Max(Math.Abs(s1), Math.Abs(s2));
            return weight;
        }

        /// <summary>
        /// Get weight of pixel at [w,h] using the Laplace operator
        /// </summary>
        /// <param name="image"> Image </param>
        /// <param name="w"> Width </param>
        /// <param name="h"> Height </par m>
        /// <returns> Weight of pixel at [w, h] </returns>
        private float GetWeightLaplace(Bitmap image, int w, int h)
        {
            Color pixel = image.GetPixel(w, h);

            float neighborhood = image.GetPixel(w - 1, h - 1).GetBrightness();
            neighborhood += image.GetPixel(w, h - 1).GetBrightness();
            neighborhood += image.GetPixel(w + 1, h - 1).GetBrightness();

            neighborhood += image.GetPixel(w - 1, h).GetBrightness();
            neighborhood += image.GetPixel(w + 1, h).GetBrightness();

            neighborhood += image.GetPixel(w - 1, h + 1).GetBrightness();
            neighborhood += image.GetPixel(w, h + 1).GetBrightness();
            neighborhood += image.GetPixel(w + 1, h + 1).GetBrightness();

            float weight = Math.Abs(pixel.GetBrightness() - 1 / 8.0f * neighborhood);

            return weight;
        }

        /// <summary>
        /// Get weight of pixel at [w,h] using the Roberts-cross operator
        /// </summary>
        /// <param name="image"> Image </param>
        /// <param name="w"> Width </param>
        /// <param name="h"> Height </par m>
        /// <returns> Weight of pixel at [w, h] </returns>
        private float GetWeightRoberts(Bitmap image, int w, int h)
        {
            Color pixel = image.GetPixel(w, h);
            Color neighbor1 = image.GetPixel(w + 1, h - 1);

            Color neighbor2 = image.GetPixel(w + 1, h);
            Color neighbor3 = image.GetPixel(w, h - 1);

            float weight = Math.Max(Math.Abs(pixel.GetBrightness() - neighbor1.GetBrightness()), Math.Abs(neighbor3.GetBrightness() - neighbor2.GetBrightness()));
            return weight;
        }

        // --------------------------------------------------------- HISTOGRAM -------------------------------------------------------------

        /// <summary>
        /// Edits histogram using Laplace operator
        /// </summary>
        /// <param name="dt"> DataFile to edit </param>
        /// <param name="edgeCase"> true if high angularity, false if low </param>
        internal void EditHistogramLaplace(DataFile dt, bool edgeCase)
        {
            double[] newHistogram = new double[1001];
            double[][] newHistogramRGB = new double[4][];
            for (int i = 0; i < newHistogramRGB.Length; i++)
                newHistogramRGB[i] = new double[1001];

            double maxValue = 0; double minValue = double.MaxValue;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w == dt.Image.Width - 1 || h == dt.Image.Height - 1))
                    {
                        weight = GetWeightLaplace(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        // float b0 = (float) Math.Sqrt(0.299 * (pixel.R / 255.0) * (pixel.R / 255.0) + 0.587 * (pixel.G / 255.0)*(pixel.G / 255.0) + 0.114 * (pixel.B / 255.0) * (pixel.B / 255.0));

                        // values are 0-255 -> map to 0-1000
                        int quantizedR = (int)(pixel.R / 255.0 * 1000);
                        int quantizedG = (int)(pixel.G / 255.0 * 1000);
                        int quantizedB = (int)(pixel.B / 255.0 * 1000);

                        if (!edgeCase)
                            weight = (1 / (1 + weight * weight));
                       

                        newHistogram[quantized] += 1 * weight;
                        newHistogramRGB[0][quantizedR] += 1 * weight;
                        newHistogramRGB[1][quantizedG] += 1 * weight;
                        newHistogramRGB[2][quantizedB] += 1 * weight;
                        
                        if (weight > maxValue)
                            maxValue = weight;
                        if (weight < minValue)
                            minValue = weight;
                    }

                }
            }

            newHistogramRGB[3] = dt.HistogramRGB[3];
            dt.HistogramRGB = newHistogramRGB;
            dt.Histogram = newHistogram;
            ComputeMatrixLaplace(dt, maxValue, minValue, edgeCase);
        }

        /// <summary>
        /// Edits histogram using Roberts-Cross operator
        /// </summary>
        /// <param name="dt"> DataFile to edit </param>
        /// <param name="edgeCase"> true if high angularity, false if low </param>
        internal void EditHistogramRoberts(DataFile dt, bool edgeCase)
        {
            double[] newHistogram = new double[1001];
            double[][] newHistogramRGB = new double[4][];
            for (int i = 0; i < newHistogramRGB.Length; i++)
                newHistogramRGB[i] = new double[1001];

            double maxValue = 0; double minValue = double.MaxValue;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    // tak brát v úvahu okolí
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(h == 0 || w == dt.Image.Width - 1))
                    {
                        weight = GetWeightRoberts(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        // values are 0-255 -> map to 0-1000
                        int quantizedR = (int)(pixel.R / 255.0 * 1000);
                        int quantizedG = (int)(pixel.G / 255.0 * 1000);
                        int quantizedB = (int)(pixel.B / 255.0 * 1000);

                        if (!edgeCase)
                            weight = (1 / (1 + weight * weight));

                        newHistogram[quantized] += 1 * weight;
                        newHistogramRGB[0][quantizedR] += 1 * weight;
                        newHistogramRGB[1][quantizedG] += 1 * weight;
                        newHistogramRGB[2][quantizedB] += 1 * weight;

                        if (weight > maxValue)
                            maxValue = weight;
                        if (weight < minValue)
                            minValue = weight;
                    }

                }
            }

            newHistogramRGB[3] = dt.HistogramRGB[3];
            dt.HistogramRGB = newHistogramRGB;
            dt.Histogram = newHistogram;
            ComputeMatrixRobertsCross(dt, maxValue, minValue, edgeCase);
        }

        /// <summary>
        /// Edits histogram using DIF operator
        /// </summary>
        /// <param name="dt"> DataFile to edit </param>
        /// <param name="edgeCase"> true if high angularity, false if low </param>
        internal void EditHistogramDIF(DataFile dt, bool edgeCase)
        {
            double[] newHistogram = new double[1001];
            double[][] newHistogramRGB = new double[4][];
            for (int i = 0; i < newHistogramRGB.Length; i++)
                newHistogramRGB[i] = new double[1001];

            double maxValue = 0; double minValue = double.MaxValue;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w >= dt.Image.Width - 2 || h >= dt.Image.Height - 2))
                    {
                        weight = GetWeightDIF(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        // values are 0-255 -> map to 0-1000
                        int quantizedR = (int)(pixel.R / 255.0 * 1000);
                        int quantizedG = (int)(pixel.G / 255.0 * 1000);
                        int quantizedB = (int)(pixel.B / 255.0 * 1000);

                        if (!edgeCase)
                            weight = (1 / (1 + weight * weight));

                        newHistogram[quantized] += 1 * weight;
                        newHistogramRGB[0][quantizedR] += 1 * weight;
                        newHistogramRGB[1][quantizedG] += 1 * weight;
                        newHistogramRGB[2][quantizedB] += 1 * weight;

                        if (weight > maxValue)
                            maxValue = weight;
                        if (weight < minValue)
                            minValue = weight;
                    }

                }
            }

            newHistogramRGB[3] = dt.HistogramRGB[3];
            dt.HistogramRGB = newHistogramRGB;
            dt.Histogram = newHistogram;
            ComputeMatrixDIF(dt, maxValue, minValue, edgeCase);
        }

        /// <summary>
        /// Edits histogram using Sobel operator
        /// </summary>
        /// <param name="dt"> DataFile to edit </param>
        /// <param name="edgeCase"> true if high angularity, false if low </param>
        internal void EditHistogramSobel(DataFile dt, bool edgeCase)
        {
            double[] newHistogram = new double[1001];
            double[][] newHistogramRGB = new double[4][];
            for (int i = 0; i < newHistogramRGB.Length; i++)
                newHistogramRGB[i] = new double[1001];

            double maxValue = 0; double minValue = double.MaxValue;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w >= dt.Image.Width - 1 || h >= dt.Image.Height - 1))
                    {
                        weight = GetWeightSobel(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        // values are 0-255 -> map to 0-1000
                        int quantizedR = (int)(pixel.R / 255.0 * 1000);
                        int quantizedG = (int)(pixel.G / 255.0 * 1000);
                        int quantizedB = (int)(pixel.B / 255.0 * 1000);

                        if (!edgeCase)
                            weight = (1 / (1 + weight * weight));

                        newHistogram[quantized] += 1 * weight;
                        newHistogramRGB[0][quantizedR] += 1 * weight;
                        newHistogramRGB[1][quantizedG] += 1 * weight;
                        newHistogramRGB[2][quantizedB] += 1 * weight;

                        if (weight > maxValue)
                            maxValue = weight;
                        if (weight < minValue)
                            minValue = weight;
                    }

                }
            }

            newHistogramRGB[3] = dt.HistogramRGB[3];
            dt.HistogramRGB = newHistogramRGB;
            dt.Histogram = newHistogram;
            ComputeMatrixDIF(dt, maxValue, minValue, edgeCase);
        }

        /// <summary>
        /// Edits histogram using Robinson operator
        /// </summary>
        /// <param name="dt"> DataFile to edit </param>
        /// <param name="edgeCase"> true if high angularity, false if low </param>
        internal void EditHistogramRobinson(DataFile dt, bool edgeCase)
        {
            double[] newHistogram = new double[1001];
            double[][] newHistogramRGB = new double[4][];
            for (int i = 0; i < newHistogramRGB.Length; i++)
                newHistogramRGB[i] = new double[1001];

            double maxValue = 0; double minValue = double.MaxValue;
            for (int w = 0; w < dt.Image.Width; w++)
            {
                for (int h = 0; h < dt.Image.Height; h++)
                {
                    float weight = 1;
                    Color pixel = dt.Image.GetPixel(w, h);

                    if (!(w == 0 || h == 0 || w >= dt.Image.Width - 1 || h >= dt.Image.Height - 1))
                    {
                        weight = GetWeightRobinson(dt.Image, w, h);
                        int quantized = (int)((pixel.GetBrightness()) * 1000);

                        // values are 0-255 -> map to 0-1000
                        int quantizedR = (int)(pixel.R / 255.0 * 1000);
                        int quantizedG = (int)(pixel.G / 255.0 * 1000);
                        int quantizedB = (int)(pixel.B / 255.0 * 1000);

                        if (!edgeCase)
                            weight = (1 / (1 + weight * weight));

                        newHistogram[quantized] += 1 * weight;
                        newHistogramRGB[0][quantizedR] += 1 * weight;
                        newHistogramRGB[1][quantizedG] += 1 * weight;
                        newHistogramRGB[2][quantizedB] += 1 * weight;

                        if (weight > maxValue)
                            maxValue = weight;
                        if (weight < minValue)
                            minValue = weight;
                    }

                }
            }

            newHistogramRGB[3] = dt.HistogramRGB[3];
            dt.HistogramRGB = newHistogramRGB;
            dt.Histogram = newHistogram;
            ComputeMatrixRobinson(dt, maxValue, minValue, edgeCase);
        }

    }

}
