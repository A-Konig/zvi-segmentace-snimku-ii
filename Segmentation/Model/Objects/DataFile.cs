﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Segmentation.Model.DataManip;

namespace Segmentation.Model.Objects
{
    /// <summary>
    /// DataFile class representing an image file
    /// </summary>
    public class DataFile
    {
        
        /// <summary> Image data </summary>
        private Bitmap image;
        public Bitmap Image { get => image; set => image = value; }

        /// <summary> Size of file in kB in format "[size] kB" </summary>
        protected string lenght;
        public string Length { get { return lenght; } set { lenght = value; } }

        /// <summary> Name of file </summary>
        protected string name;
        public string Name { get { return name; } set { name = value; /* Set(ref name, value, new string[] { nameof(FullName) }); */ } }

        /// <summary> Extension of file </summary>
        protected string extension;
        public string Extension { get { return extension; } set { extension = value; /* Set(ref extension, value, new string[] { nameof(FullName)}); */ } }

        /// <summary> Is file valid </summary>
        private bool valid;
        public bool Valid { get => valid; set => valid = value; }

        /// <summary> Histogram </summary>
        private double[] histogram;
        public double[] Histogram { get => histogram; set => histogram = value; }

        /// <summary> 2D histogram </summary>
        private double[,] histogram2D;
        public double[,] Histogram2D { get => histogram2D; set => histogram2D = value; }

        /// <summary> Histograms for R, G, B and relative </summary>
        private double[][] histogramRGB;
        public double[][] HistogramRGB { get => histogramRGB; set => histogramRGB = value; }

        /// <summary> Type of histogram - original, laplace, roberts-cross or DIF, high/low angularity, depends on selected edits </summary>
        private string histogramType;
        public string HistogramType { get => histogramType; set => histogramType = value; }

        /// <summary> Angularity matrix </summary>
        private int[,] matrix;
        public int[,] Matrix { get => matrix; set => matrix = value; }

        /// <summary> Minimum and maximum used to compute the matrix </summary>
        private double[] minMaxInMatrix;
        public double[] MinMaxInMatrix { get => minMaxInMatrix; set => minMaxInMatrix = value; }

        /// <summary> Full name of file (name + extension) </summary>
        public string FullName { get { return $"{name}{extension}"; } }

        /// <summary> Export information </summary>
        private string exportInfo;
        public string ExportInfo { get => exportInfo; set => exportInfo = value; }

        /// <summary> List of images needed to create the result </summary>
        private List<DataFile> preProcessFiles;
        public List<DataFile> PreProcessFiles { get => preProcessFiles; set => preProcessFiles = value; }

        /// <summary>
        /// Constructor
        /// </summary>
        public DataFile() { PreProcessFiles = new List<DataFile>(); }

        /// <summary>
        /// Constructor creating a DataFile from path
        /// </summary>
        /// <param name="path"> Path to file </param>
        public DataFile(string path)
        {
            if (FileManip.FileLenght(path) < Config.Cap)
            {
                image = FileManip.GetImageFromFile(path);
                name = FileManip.GetName(path);
                extension = FileManip.GetExtension(path);
            }

            // data loaded and correct extension
            if ( (image != null && name != null && extension != null) &&
               (extension.ToLower().Equals(".bmp") || extension.ToLower().Equals(".png") ||
                extension.ToLower().Equals(".jpg") || extension.ToLower().Equals(".jpeg")))
            {
                Valid = true;
                lenght = $"{FileManip.GetSize(path)}";
                HistogramType = "original";
                CalculateHistogram(true);
                matrix = new int[1,1];
                minMaxInMatrix = new double[] { 0, 0 };

            }
        }

        /// <summary>
        /// Calculates histogram from image file
        /// </summary>
        /// <param name="calc2D"> If true calculate 2D histogram </param>
        public void CalculateHistogram(bool calc2D = false, int startThreshold = 0, int endThreshold = 1001)
        {
            // since brightness is 0-1 -> values of 0-1000
            histogram = new double[1001];
            histogramRGB = new double[4][];
            for (int i = 0; i < histogramRGB.Length; i++)
                histogramRGB[i] = new double[1001];

            double[,] vals = new double[1001, 1001];

            float sum = 0;
            for (int w = 0; w < image.Width; w++)
            {
                for (int h = 0; h < image.Height; h++)
                {
                    Color pixel = image.GetPixel(w, h);
                    float brightness = pixel.GetBrightness();

                    int quantized = (int)(brightness * 1000);

                    if (quantized > endThreshold || quantized < startThreshold)
                        continue;

                    histogram[quantized]++;

                    // values are 0-255 -> map to 0-1000
                    int quantizedR = (int)(pixel.R / 255.0 * 1000);
                    int quantizedG = (int)(pixel.G / 255.0 * 1000);
                    int quantizedB = (int)(pixel.B / 255.0 * 1000);

                    sum++;

                    histogramRGB[0][quantizedR]++;
                    histogramRGB[1][quantizedG]++;
                    histogramRGB[2][quantizedB]++;

                    if (calc2D)
                    {
                        if (w == 0 || h == 0 || w == Image.Width - 1 || h == Image.Height - 1)
                            continue;

                        float n = Image.GetPixel(w - 1, h - 1).GetBrightness();
                        n += Image.GetPixel(w - 1, h).GetBrightness();
                        n += Image.GetPixel(w - 1, h + 1).GetBrightness();
                        n += Image.GetPixel(w, h - 1).GetBrightness();
                        n += Image.GetPixel(w, h + 1).GetBrightness();
                        n += Image.GetPixel(w + 1, h - 1).GetBrightness();
                        n += Image.GetPixel(w + 1, h).GetBrightness();
                        n += Image.GetPixel(w + 1, h + 1).GetBrightness();
                        float avgN = n / 8.0f;

                        // indices
                        int indX = (int)(pixel.GetBrightness() * 1000);
                        int indY = (int)(avgN * 1000);
                        vals[indX, indY]++;
                    }
                }
            }

            // relative histogram
            double currSum = 0;
            for (int i = 0; i < histogram.Length; i++)
            {
                currSum += histogram[i];
                histogramRGB[3][i] = currSum / sum;
            }

            matrix = new int[1,1];
            minMaxInMatrix = new double[] { 0, 0 };
            if (calc2D)
                histogram2D = vals;
        }

        /// <summary>
        /// To String of DataFile
        /// </summary>
        /// <returns> String </returns>
        public override string ToString()
        {
            return $"{Name}{Extension}[{lenght}]";
        }

        /// <summary>
        /// Creates a copy of DataFile
        /// </summary>
        /// <returns></returns>
        public DataFile DeepCopy()
        {
            DataFile dt = new DataFile();

            dt.histogram = new double[histogram.Length];
            for (int i = 0; i < histogram.Length; i++)
                dt.histogram[i] = histogram[i];

            dt.histogramRGB = new double[histogramRGB.Length][];
            for (int i = 0; i < histogramRGB.Length; i++)
            {
                dt.histogramRGB[i] = new double[histogramRGB[i].Length];
                for (int j = 0; j < histogramRGB[i].Length; j++)
                    dt.histogramRGB[i][j] = histogramRGB[i][j];
            }

            dt.minMaxInMatrix = new double[minMaxInMatrix.Length];
            for (int i = 0; i < minMaxInMatrix.Length; i++)
                dt.minMaxInMatrix[i] = minMaxInMatrix[i];

            dt.image = new Bitmap(image);
            dt.lenght = lenght;
            dt.extension = extension;
            dt.name = name;
            dt.valid = valid;

            return dt;
        }
    }
}


/*
  #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Notifikace zmeny vlastnosti propertyName instance.
        /// </summary>
        /// <param name="propertyName">Nazev vlastnosti u ktere doslo ke zmene</param>
        void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// Nastaveni nove hodnoty vlastnosti a notifikace.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldVal">Puvodni hodnota</param>
        /// <param name="newVal">Nova hodnota</param>
        /// <param name="propertyName">Jmeno vlastnosti</param>
        void Set<T>(ref T oldVal, T newVal, string[] dependentProperties = null, [CallerMemberName]string propertyName = "")
        {
            if (newVal == null || !newVal.Equals(oldVal))
            {
                oldVal = newVal;
                NotifyPropertyChanged(propertyName);
                if (dependentProperties != null)
                {
                    foreach (var dependentProperty in dependentProperties)
                        NotifyPropertyChanged(dependentProperty);
                }
            }
        }
        #endregion
*/

/*
 internal void EditHistogramRoberts(bool edgeCase)
 {
     double[] newHistogram = new double[1001];
     double maxValue = 0;
     for (int w = 0; w < image.Width; w++)
     {
         for (int h = 0; h < image.Height; h++)
         {
             // tak brát v úvahu okolí
             float weight = 1;
             Color pixel = image.GetPixel(w, h);

             if (h == 0 || w == image.Width - 1)
                 weight = 1;
             else
             {
                 Color neighbor1 = image.GetPixel(w + 1, h - 1);

                 Color neighbor2 = image.GetPixel(w + 1, h);
                 Color neighbor3 = image.GetPixel(w, h - 1);

                 weight = Math.Max(Math.Abs(pixel.GetBrightness() - neighbor1.GetBrightness()), Math.Abs(neighbor3.GetBrightness() - neighbor2.GetBrightness()));
             }

             int quantized = (int)((pixel.GetBrightness()) * 1000);

             if (edgeCase)
             {
                 newHistogram[quantized] += 1 * weight;
                 if (weight > maxValue)
                     maxValue = weight;
             }
             else
                 newHistogram[quantized] += 1 * (1 / (1 + weight * weight));
         }
     }

     histogram = newHistogram;
     ComputeMatrix(maxValue, edgeCase);
 }

 private void ComputeMatrix(double maxValue, bool edgeCase)
 {
     matrix = new int[1001 * 1001];

     double step = maxValue / 1000;
     for (int w = 0; w < image.Width; w++)
     {
         for (int h = 0; h < image.Height; h++)
         {
             // tak brát v úvahu okolí
             float weight = 1;
             Color pixel = image.GetPixel(w, h);

             if (!(h == 0 || w == image.Width - 1))
             {
                 Color neighbor1 = image.GetPixel(w + 1, h - 1);
                 Color neighbor2 = image.GetPixel(w + 1, h);
                 Color neighbor3 = image.GetPixel(w, h - 1);
                 weight = Math.Max(Math.Abs(pixel.GetBrightness() - neighbor1.GetBrightness()), Math.Abs(neighbor3.GetBrightness() - neighbor2.GetBrightness()));
             }

             int quantized = (int)((pixel.GetBrightness()) * 1000);

             if (edgeCase)
             {
                 int ang = (int)(weight / step);
                 matrix[1001 * ang + quantized]++;

                 if (weight > maxValue)
                     maxValue = weight;
             }
             else
             {
                 int ang = (int)(weight * 1000);
                 matrix[1001 * ang + quantized]++;
             }
         }
     }
 }

 internal void EditHistogramLaplace(bool edgeCase)
 {
     double[] newHistogram = new double[1001];
     double maxValue = 0;
     for (int w = 0; w < image.Width; w++)
     {
         for (int h = 0; h < image.Height; h++)
         {
             // tak brát v úvahu okolí
             float weight = 1;
             Color pixel = image.GetPixel(w, h);

             if (w == 0 || h == 0 || w == image.Width - 1 || h == image.Height - 1)
                 weight = 1;
             else
             {
                 float neighborhood = image.GetPixel(w - 1, h - 1).GetBrightness();
                 neighborhood += image.GetPixel(w, h - 1).GetBrightness();
                 neighborhood += image.GetPixel(w + 1, h - 1).GetBrightness();

                 neighborhood += image.GetPixel(w - 1, h).GetBrightness();
                 neighborhood += image.GetPixel(w + 1, h).GetBrightness();

                 neighborhood += image.GetPixel(w - 1, h + 1).GetBrightness();
                 neighborhood += image.GetPixel(w, h + 1).GetBrightness();
                 neighborhood += image.GetPixel(w + 1, h + 1).GetBrightness();

                 weight = Math.Abs(pixel.GetBrightness() - 1 / 8.0f * neighborhood);
             }

             int quantized = (int)((pixel.GetBrightness()) * 1000);

             if (edgeCase)
             {
                 newHistogram[quantized] += 1 * weight;
                 if (weight > maxValue)
                     maxValue = weight;
             }
             else
                 newHistogram[quantized] += 1 * (1 / (1 + weight * weight));
         }
     }



     histogram = newHistogram;
     ComputeMatrix(maxValue, edgeCase);
 }

 internal void EditHistogramDIF(bool edgeCase)
 {
     double[] newHistogram = new double[1001];
     double maxValue = 0;
     for (int w = 0; w < image.Width; w++)
     {
         for (int h = 0; h < image.Height; h++)
         {
             // tak brát v úvahu okolí
             float weight = 1;
             Color pixel = image.GetPixel(w, h);

             if (w == 0 || h == 0 || w >= image.Width - 2 || h >= image.Height - 2)
                 weight = 1;
             else
             {
                 float s1 = pixel.GetBrightness();
                 s1 += image.GetPixel(w, h - 1).GetBrightness();
                 s1 += image.GetPixel(w + 1, h - 1).GetBrightness();
                 s1 += image.GetPixel(w + 1, h).GetBrightness();
                 s1 -= image.GetPixel(w, h + 1).GetBrightness();
                 s1 -= image.GetPixel(w, h + 2).GetBrightness();
                 s1 -= image.GetPixel(w + 1, h + 1).GetBrightness();
                 s1 -= image.GetPixel(w + 1, h + 2).GetBrightness();

                 float s2 = pixel.GetBrightness();
                 s2 += image.GetPixel(w - 1, h).GetBrightness();
                 s2 += image.GetPixel(w - 1, h + 1).GetBrightness();
                 s2 += image.GetPixel(w, h + 1).GetBrightness();
                 s2 -= image.GetPixel(w + 1, h).GetBrightness();
                 s2 -= image.GetPixel(w + 2, h).GetBrightness();
                 s2 -= image.GetPixel(w + 1, h + 1).GetBrightness();
                 s2 -= image.GetPixel(w + 2, h + 1).GetBrightness();

                 weight = Math.Max(Math.Abs(s1), Math.Abs(s2));
             }

             int quantized = (int)((pixel.GetBrightness()) * 1000);

             if (edgeCase)
             {
                 newHistogram[quantized] += 1 * weight;
                 if (weight > maxValue)
                     maxValue = weight;
             }
             else
                 newHistogram[quantized] += 1 * (1 / (1 + weight * weight));
         }
     }

     histogram = newHistogram;
     ComputeMatrix(maxValue, edgeCase);
 }
 */