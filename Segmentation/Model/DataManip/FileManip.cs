﻿using Segmentation.Model.Objects;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace Segmentation.Model.DataManip
{
    /// <summary>
    /// Class for the manipulation of files and their properties
    /// </summary>
    class FileManip
    {
        /// <summary>
        /// Does file exist
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <returns>True if file exists false if not</returns>
        public static bool FileExists(string path)
        {
            return File.Exists(path);
        }

        /// <summary>
        /// Reads image from a file on disk
        /// </summary>
        /// <param name="path"> Path to file </param>
        /// <returns> Bitmap with image </returns>
        public static Bitmap GetImageFromFile(string path)
        {
            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (!File.Exists(path) || !(FileLenght(path) < Config.Cap))
                    return null;

                return new Bitmap(path);
            } catch
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a folder if it doesnt exist
        /// </summary>
        /// <param name="path"> Path to folder </param>
        /// <returns> True if successful, false if not </returns>
        internal static bool CreateFolder(string path)
        {
            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return true;
            } catch
            {
                return false;
            }
        }

        /// <summary>
        /// Saves information about input file into a txt file
        /// </summary>
        /// <param name="dataFile"> File </param>
        /// <param name="edited"> Edited file </param>
        /// <param name="name"> Name of output file </param>
        /// <returns> Name of output file if successfull, null if not successfull </returns>
        internal static string SaveInfoAboutFile(DataFile dataFile, DataFile edited, string name)
        {
            // no dataFile or no output path
            if (dataFile == null || !Directory.Exists(Config.OutputPath))
                return null;

            // output name
            string path = Config.OutputPath + "/" + dataFile.Name + "_out" + ".txt";
            if (name != null)
                path = Config.OutputPath + "/" + name + ".txt";

            var sb = new StringBuilder(string.Empty);

            sb.Append($"Original image: {dataFile.FullName} \n");

            // info about used threshold
            sb.Append(edited.ExportInfo);

            // histogram
            sb.Append("\n");
            sb.Append($"Histogram: {dataFile.HistogramType} \n");
            for (int i = 0; i < dataFile.Histogram.Length; i++)
                sb.Append($"{string.Format("{0:N4}", i / 1000.0)} \t {dataFile.Histogram[i]} \n");

            // angularity matrix
            sb.Append("\n");
            sb.Append($"Angularity matrix: {dataFile.HistogramType} \n");
            sb.Append($"Size of matrix: [{dataFile.Matrix.GetLength(0)}, {dataFile.Matrix.GetLength(1)}]\n");
            sb.Append("x values: <0, 1>, y values: <" + dataFile.MinMaxInMatrix[0] + ", " + dataFile.MinMaxInMatrix[1] + ">\n");
            for (int h = dataFile.Matrix.GetLength(1) - 1; h >= 0; h--)
            {
                for (int w = 0; w < dataFile.Matrix.GetLength(0); w++)
                {
                    sb.Append(dataFile.Matrix[w, h] + " ");
                }
                sb.Append("\n");
            }

            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (File.Exists(path))
                    File.Delete(path);

                File.WriteAllText(path, sb.ToString());
            }
            catch
            {
                return null;
            }

            return path;
        }

        /// <summary>
        /// Gets the extension of file
        /// </summary>
        /// <param name="path"> Path </param>
        /// <returns> Extension or null if file doesnt exist </returns>
        internal static string GetExtension(string path)
        {
            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (!File.Exists(path))
                    return null;

                return Path.GetExtension(path);
            } catch
            {
                return null;
            }
        }

        /// <summary>
        /// Write file
        /// </summary>
        /// <param name="data"> DataFile to write into an image file </param>
        /// <returns> Name of file if successfull, null if not </returns>
        internal static string WriteFile(DataFile data, string name)
        {
            // no file or no output path
            if (data == null || data.Image == null || !Directory.Exists(Config.OutputPath))
                return null;

            string path = Config.OutputPath + "/" + data.FullName;
            if (name != null)
                path = Config.OutputPath + "/" + name + data.Extension;

            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (File.Exists(path))
                    File.Delete(path);

                // decide on data format
                switch (data.Extension.Trim().ToLower())
                {
                    case ".bmp":
                        {
                            data.Image.Save(path, ImageFormat.Bmp);
                            break;
                        }
                    case ".jpg":
                        {
                            data.Image.Save(path, ImageFormat.Jpeg);
                            break;
                        }
                    case ".jpeg":
                        {
                            data.Image.Save(path, ImageFormat.Jpeg);
                            break;
                        }
                    case ".png":
                        {
                            data.Image.Save(path, ImageFormat.Png);
                            break;
                        }
                }
            }
            catch
            {
                return null;
            }

            return path;
        }

        /// <summary>
        /// Gets name of file without extension
        /// </summary>
        /// <param name="path"> Path </param>
        /// <returns> Name or null if file doesnt exist </returns>
        internal static string GetName(string path)
        {
            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (!File.Exists(path))
                    return null;

                return Path.GetFileNameWithoutExtension(path);
            } catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets size of file in kB in format "[size] kB"
        /// </summary>
        /// <param name="path"> Path </param>
        /// <returns> String with the size of file </returns>
        internal static string GetSize(string path)
        {
            int l = FileLenght(path);

            if (l == -1)
                return "0 kB";

            int len = (int)Math.Ceiling(l / 1000.0);
            return $"{len} kB";
        }

        /// <summary>
        /// Gets length of file in bytes
        /// </summary>
        /// <param name="path"> Path to file </param>
        /// <returns> Length of file </returns>
        internal static int FileLenght(string path)
        {
            // catch exceptions if path doesnt exist or denied access
            try
            {
                if (!File.Exists(path))
                    return -1;

                FileInfo fi = new FileInfo(path);
                return (int)fi.Length;
            } catch
            {
                return -1;
            }
        }
    }
}