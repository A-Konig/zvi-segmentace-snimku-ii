﻿using Segmentation.Model.DataManip;
using Segmentation.Model.Processors;
using Segmentation.Presenter;
using System;
using System.Windows.Forms;

namespace Segmentation
{
    /// <summary>
    /// Main class
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // create folders
            if (!FileManip.CreateFolder(Application.StartupPath + "/output"))
            {
                Console.WriteLine("Unable to create an output folder");
                return;
            }

            // configure input/output
            Config.InputPath = Application.StartupPath;
            Config.OutputPath = Application.StartupPath + "/output";

            // create main controllers
            var view = new SegmentationView();
            var handler = new Processor();
            var presenter = new SteganographyPresenter(handler, view);

            Application.Run(view);
        }
    }
}