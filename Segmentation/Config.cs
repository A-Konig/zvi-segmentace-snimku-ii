﻿
namespace Segmentation
{
    /// <summary>
    /// Configuration
    /// </summary>
    class Config
    {
        /// <summary> Input folder path </summary>
        protected static string inputPath;
        public static string InputPath { get { return inputPath; } set { inputPath = value; } }

        /// <summary> Output folder path </summary>
        protected static string outputPath;
        public static string OutputPath { get { return outputPath; } set { outputPath = value; } }

        /// <summary> Max size of file input in bytes </summary>
        protected static int cap = 500000000;
        public static int Cap { get { return cap; } set { cap = value; } }

        /// <summary> Depth of history </summary>
        private static int historyDepth = 3;
        public static int HistoryDepth { get => historyDepth; set => historyDepth = value; }

    }
}
