﻿namespace Segmentation
{
    partial class SegmentationView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SegmentationView));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoOneStepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.confOutputMN = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.matrixBT = new System.Windows.Forms.Button();
            this.pathTXT = new System.Windows.Forms.Label();
            this.deleteBT = new System.Windows.Forms.Button();
            this.extractLBL = new System.Windows.Forms.Label();
            this.pathLBL = new System.Windows.Forms.Label();
            this.nameUpTXT = new System.Windows.Forms.Label();
            this.nameUpLBL = new System.Windows.Forms.Label();
            this.sizeUpLBL = new System.Windows.Forms.Label();
            this.sizeUpTXT = new System.Windows.Forms.Label();
            this.histogramBT = new System.Windows.Forms.Button();
            this.dbNameERR = new System.Windows.Forms.ErrorProvider(this.components);
            this.inputERR = new System.Windows.Forms.ErrorProvider(this.components);
            this.beforePic = new System.Windows.Forms.PictureBox();
            this.BeforeGroup = new System.Windows.Forms.GroupBox();
            this.beforePanel = new System.Windows.Forms.Panel();
            this.AfterGroup = new System.Windows.Forms.GroupBox();
            this.afterPanel = new System.Windows.Forms.Panel();
            this.afterPic = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.outputNameBox = new System.Windows.Forms.TextBox();
            this.ExportInfoBT = new System.Windows.Forms.Button();
            this.ExportBT = new System.Windows.Forms.Button();
            this.stateLBL = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.sauvolaTXT = new System.Windows.Forms.TextBox();
            this.sauvolaCMB = new System.Windows.Forms.ComboBox();
            this.sauvolaBT = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.manTXT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.applyManBT = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.probTXT = new System.Windows.Forms.TextBox();
            this.applyPercBT = new System.Windows.Forms.Button();
            this.tab2 = new System.Windows.Forms.TabPage();
            this.rotsuTXT = new System.Windows.Forms.TextBox();
            this.applyRotsuBT = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.applyCharBT = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.applyEntrBT = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.applyOtsuBT = new System.Windows.Forms.Button();
            this.applyMinBT = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.applyOtsu2BT = new System.Windows.Forms.Button();
            this.angCHCK = new System.Windows.Forms.CheckBox();
            this.revertHistoBT = new System.Windows.Forms.Button();
            this.methCMB = new System.Windows.Forms.ComboBox();
            this.applyHistoBT = new System.Windows.Forms.Button();
            this.presCHCK = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.historyBT = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.rmBgRotsuCHCK = new System.Windows.Forms.CheckBox();
            this.menu.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbNameERR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputERR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beforePic)).BeginInit();
            this.BeforeGroup.SuspendLayout();
            this.beforePanel.SuspendLayout();
            this.AfterGroup.SuspendLayout();
            this.afterPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.afterPic)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tab2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.editToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menu.Size = new System.Drawing.Size(1168, 24);
            this.menu.TabIndex = 3;
            this.menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFileToolStripMenuItem});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // loadFileToolStripMenuItem
            // 
            this.loadFileToolStripMenuItem.Name = "loadFileToolStripMenuItem";
            this.loadFileToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.loadFileToolStripMenuItem.Text = "Load file";
            this.loadFileToolStripMenuItem.Click += new System.EventHandler(this.loadFileToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoOneStepToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoOneStepToolStripMenuItem
            // 
            this.undoOneStepToolStripMenuItem.Name = "undoOneStepToolStripMenuItem";
            this.undoOneStepToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.undoOneStepToolStripMenuItem.Text = "Undo one step";
            this.undoOneStepToolStripMenuItem.Click += new System.EventHandler(this.undoOneStepToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.confOutputMN});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // confOutputMN
            // 
            this.confOutputMN.Name = "confOutputMN";
            this.confOutputMN.Size = new System.Drawing.Size(197, 22);
            this.confOutputMN.Text = "Configure outputfolder";
            this.confOutputMN.Click += new System.EventHandler(this.confOutputMN_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.matrixBT);
            this.groupBox3.Controls.Add(this.pathTXT);
            this.groupBox3.Controls.Add(this.deleteBT);
            this.groupBox3.Controls.Add(this.extractLBL);
            this.groupBox3.Controls.Add(this.pathLBL);
            this.groupBox3.Controls.Add(this.nameUpTXT);
            this.groupBox3.Controls.Add(this.nameUpLBL);
            this.groupBox3.Controls.Add(this.sizeUpLBL);
            this.groupBox3.Controls.Add(this.sizeUpTXT);
            this.groupBox3.Controls.Add(this.histogramBT);
            this.groupBox3.Location = new System.Drawing.Point(15, 30);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(262, 121);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Input file";
            // 
            // matrixBT
            // 
            this.matrixBT.Location = new System.Drawing.Point(104, 89);
            this.matrixBT.Name = "matrixBT";
            this.matrixBT.Size = new System.Drawing.Size(74, 23);
            this.matrixBT.TabIndex = 17;
            this.matrixBT.Text = "Show matrix";
            this.matrixBT.UseVisualStyleBackColor = true;
            this.matrixBT.Click += new System.EventHandler(this.matrixBT_Click);
            // 
            // pathTXT
            // 
            this.pathTXT.AutoSize = true;
            this.pathTXT.Location = new System.Drawing.Point(55, 17);
            this.pathTXT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pathTXT.Name = "pathTXT";
            this.pathTXT.Size = new System.Drawing.Size(45, 13);
            this.pathTXT.TabIndex = 16;
            this.pathTXT.Text = "< N/A >";
            // 
            // deleteBT
            // 
            this.deleteBT.Location = new System.Drawing.Point(181, 89);
            this.deleteBT.Name = "deleteBT";
            this.deleteBT.Size = new System.Drawing.Size(75, 23);
            this.deleteBT.TabIndex = 5;
            this.deleteBT.Text = "Delete";
            this.deleteBT.UseVisualStyleBackColor = true;
            this.deleteBT.Click += new System.EventHandler(this.deleteBT_Click_1);
            // 
            // extractLBL
            // 
            this.extractLBL.AutoSize = true;
            this.extractLBL.Location = new System.Drawing.Point(116, 20);
            this.extractLBL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.extractLBL.Name = "extractLBL";
            this.extractLBL.Size = new System.Drawing.Size(0, 13);
            this.extractLBL.TabIndex = 15;
            // 
            // pathLBL
            // 
            this.pathLBL.AutoSize = true;
            this.pathLBL.Location = new System.Drawing.Point(4, 17);
            this.pathLBL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pathLBL.Name = "pathLBL";
            this.pathLBL.Size = new System.Drawing.Size(29, 13);
            this.pathLBL.TabIndex = 7;
            this.pathLBL.Text = "Path";
            // 
            // nameUpTXT
            // 
            this.nameUpTXT.AutoSize = true;
            this.nameUpTXT.Location = new System.Drawing.Point(55, 39);
            this.nameUpTXT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameUpTXT.Name = "nameUpTXT";
            this.nameUpTXT.Size = new System.Drawing.Size(45, 13);
            this.nameUpTXT.TabIndex = 11;
            this.nameUpTXT.Text = "< N/A >";
            // 
            // nameUpLBL
            // 
            this.nameUpLBL.AutoSize = true;
            this.nameUpLBL.Location = new System.Drawing.Point(3, 39);
            this.nameUpLBL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameUpLBL.Name = "nameUpLBL";
            this.nameUpLBL.Size = new System.Drawing.Size(35, 13);
            this.nameUpLBL.TabIndex = 7;
            this.nameUpLBL.Text = "Name";
            // 
            // sizeUpLBL
            // 
            this.sizeUpLBL.AutoSize = true;
            this.sizeUpLBL.Location = new System.Drawing.Point(4, 64);
            this.sizeUpLBL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sizeUpLBL.Name = "sizeUpLBL";
            this.sizeUpLBL.Size = new System.Drawing.Size(27, 13);
            this.sizeUpLBL.TabIndex = 10;
            this.sizeUpLBL.Text = "Size";
            // 
            // sizeUpTXT
            // 
            this.sizeUpTXT.AutoSize = true;
            this.sizeUpTXT.Location = new System.Drawing.Point(55, 64);
            this.sizeUpTXT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sizeUpTXT.Name = "sizeUpTXT";
            this.sizeUpTXT.Size = new System.Drawing.Size(29, 13);
            this.sizeUpTXT.TabIndex = 7;
            this.sizeUpTXT.Text = "0 kB";
            // 
            // histogramBT
            // 
            this.histogramBT.Location = new System.Drawing.Point(4, 89);
            this.histogramBT.Name = "histogramBT";
            this.histogramBT.Size = new System.Drawing.Size(96, 23);
            this.histogramBT.TabIndex = 4;
            this.histogramBT.Text = "Show histogram";
            this.histogramBT.UseVisualStyleBackColor = true;
            this.histogramBT.Click += new System.EventHandler(this.histogramBT_Click);
            // 
            // dbNameERR
            // 
            this.dbNameERR.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.dbNameERR.ContainerControl = this;
            // 
            // inputERR
            // 
            this.inputERR.ContainerControl = this;
            // 
            // beforePic
            // 
            this.beforePic.Location = new System.Drawing.Point(3, 3);
            this.beforePic.Name = "beforePic";
            this.beforePic.Size = new System.Drawing.Size(411, 494);
            this.beforePic.TabIndex = 5;
            this.beforePic.TabStop = false;
            // 
            // BeforeGroup
            // 
            this.BeforeGroup.Controls.Add(this.beforePanel);
            this.BeforeGroup.Location = new System.Drawing.Point(282, 30);
            this.BeforeGroup.Name = "BeforeGroup";
            this.BeforeGroup.Size = new System.Drawing.Size(429, 523);
            this.BeforeGroup.TabIndex = 6;
            this.BeforeGroup.TabStop = false;
            this.BeforeGroup.Text = "Before";
            // 
            // beforePanel
            // 
            this.beforePanel.Controls.Add(this.beforePic);
            this.beforePanel.Location = new System.Drawing.Point(6, 17);
            this.beforePanel.Name = "beforePanel";
            this.beforePanel.Size = new System.Drawing.Size(417, 500);
            this.beforePanel.TabIndex = 6;
            // 
            // AfterGroup
            // 
            this.AfterGroup.Controls.Add(this.afterPanel);
            this.AfterGroup.Location = new System.Drawing.Point(717, 30);
            this.AfterGroup.Name = "AfterGroup";
            this.AfterGroup.Size = new System.Drawing.Size(439, 523);
            this.AfterGroup.TabIndex = 7;
            this.AfterGroup.TabStop = false;
            this.AfterGroup.Text = "After";
            // 
            // afterPanel
            // 
            this.afterPanel.Controls.Add(this.afterPic);
            this.afterPanel.Location = new System.Drawing.Point(6, 17);
            this.afterPanel.Name = "afterPanel";
            this.afterPanel.Size = new System.Drawing.Size(427, 500);
            this.afterPanel.TabIndex = 9;
            // 
            // afterPic
            // 
            this.afterPic.Location = new System.Drawing.Point(3, 3);
            this.afterPic.Name = "afterPic";
            this.afterPic.Size = new System.Drawing.Size(421, 494);
            this.afterPic.TabIndex = 0;
            this.afterPic.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(841, 582);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Output name";
            // 
            // outputNameBox
            // 
            this.outputNameBox.Location = new System.Drawing.Point(913, 575);
            this.outputNameBox.Margin = new System.Windows.Forms.Padding(2);
            this.outputNameBox.Name = "outputNameBox";
            this.outputNameBox.Size = new System.Drawing.Size(247, 20);
            this.outputNameBox.TabIndex = 31;
            // 
            // ExportInfoBT
            // 
            this.ExportInfoBT.Location = new System.Drawing.Point(1003, 600);
            this.ExportInfoBT.Name = "ExportInfoBT";
            this.ExportInfoBT.Size = new System.Drawing.Size(75, 23);
            this.ExportInfoBT.TabIndex = 30;
            this.ExportInfoBT.Text = "Export info";
            this.ExportInfoBT.UseVisualStyleBackColor = true;
            this.ExportInfoBT.Click += new System.EventHandler(this.ExportInfoBT_Click);
            // 
            // ExportBT
            // 
            this.ExportBT.Location = new System.Drawing.Point(1084, 600);
            this.ExportBT.Name = "ExportBT";
            this.ExportBT.Size = new System.Drawing.Size(75, 23);
            this.ExportBT.TabIndex = 0;
            this.ExportBT.Text = "Save image";
            this.ExportBT.UseVisualStyleBackColor = true;
            this.ExportBT.Click += new System.EventHandler(this.ExportBT_Click);
            // 
            // stateLBL
            // 
            this.stateLBL.AutoSize = true;
            this.stateLBL.Location = new System.Drawing.Point(11, 613);
            this.stateLBL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.stateLBL.Name = "stateLBL";
            this.stateLBL.Size = new System.Drawing.Size(0, 13);
            this.stateLBL.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.angCHCK);
            this.groupBox1.Controls.Add(this.revertHistoBT);
            this.groupBox1.Controls.Add(this.methCMB);
            this.groupBox1.Controls.Add(this.applyHistoBT);
            this.groupBox1.Controls.Add(this.presCHCK);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Location = new System.Drawing.Point(14, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 451);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(94, 109);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "METHODS";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tab2);
            this.tabControl2.Location = new System.Drawing.Point(5, 128);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(252, 314);
            this.tabControl2.TabIndex = 34;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.sauvolaTXT);
            this.tabPage3.Controls.Add(this.sauvolaCMB);
            this.tabPage3.Controls.Add(this.sauvolaBT);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.manTXT);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.applyManBT);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.probTXT);
            this.tabPage3.Controls.Add(this.applyPercBT);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(244, 288);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Manual";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // sauvolaTXT
            // 
            this.sauvolaTXT.Location = new System.Drawing.Point(8, 170);
            this.sauvolaTXT.Margin = new System.Windows.Forms.Padding(2);
            this.sauvolaTXT.Name = "sauvolaTXT";
            this.sauvolaTXT.Size = new System.Drawing.Size(123, 20);
            this.sauvolaTXT.TabIndex = 38;
            // 
            // sauvolaCMB
            // 
            this.sauvolaCMB.FormattingEnabled = true;
            this.sauvolaCMB.Location = new System.Drawing.Point(136, 144);
            this.sauvolaCMB.Name = "sauvolaCMB";
            this.sauvolaCMB.Size = new System.Drawing.Size(100, 21);
            this.sauvolaCMB.TabIndex = 36;
            // 
            // sauvolaBT
            // 
            this.sauvolaBT.Location = new System.Drawing.Point(161, 170);
            this.sauvolaBT.Name = "sauvolaBT";
            this.sauvolaBT.Size = new System.Drawing.Size(75, 23);
            this.sauvolaBT.TabIndex = 37;
            this.sauvolaBT.Text = "Apply";
            this.sauvolaBT.UseVisualStyleBackColor = true;
            this.sauvolaBT.Click += new System.EventHandler(this.sauvolaBT_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 18);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Manual threshold";
            // 
            // manTXT
            // 
            this.manTXT.Location = new System.Drawing.Point(115, 14);
            this.manTXT.Margin = new System.Windows.Forms.Padding(2);
            this.manTXT.Name = "manTXT";
            this.manTXT.Size = new System.Drawing.Size(121, 20);
            this.manTXT.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 147);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "Niblack method - subtype";
            // 
            // applyManBT
            // 
            this.applyManBT.Location = new System.Drawing.Point(163, 39);
            this.applyManBT.Name = "applyManBT";
            this.applyManBT.Size = new System.Drawing.Size(75, 23);
            this.applyManBT.TabIndex = 2;
            this.applyManBT.Text = "Apply";
            this.applyManBT.UseVisualStyleBackColor = true;
            this.applyManBT.Click += new System.EventHandler(this.ApplyBT_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 83);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Percentage of object";
            // 
            // probTXT
            // 
            this.probTXT.Location = new System.Drawing.Point(115, 78);
            this.probTXT.Margin = new System.Windows.Forms.Padding(2);
            this.probTXT.Name = "probTXT";
            this.probTXT.Size = new System.Drawing.Size(121, 20);
            this.probTXT.TabIndex = 18;
            // 
            // applyPercBT
            // 
            this.applyPercBT.Location = new System.Drawing.Point(161, 103);
            this.applyPercBT.Name = "applyPercBT";
            this.applyPercBT.Size = new System.Drawing.Size(75, 23);
            this.applyPercBT.TabIndex = 19;
            this.applyPercBT.Text = "Apply";
            this.applyPercBT.UseVisualStyleBackColor = true;
            this.applyPercBT.Click += new System.EventHandler(this.ApplyProbBT_Click);
            // 
            // tab2
            // 
            this.tab2.Controls.Add(this.rmBgRotsuCHCK);
            this.tab2.Controls.Add(this.rotsuTXT);
            this.tab2.Controls.Add(this.applyRotsuBT);
            this.tab2.Controls.Add(this.label3);
            this.tab2.Controls.Add(this.applyCharBT);
            this.tab2.Controls.Add(this.label1);
            this.tab2.Controls.Add(this.applyEntrBT);
            this.tab2.Controls.Add(this.label12);
            this.tab2.Controls.Add(this.label9);
            this.tab2.Controls.Add(this.applyOtsuBT);
            this.tab2.Controls.Add(this.applyMinBT);
            this.tab2.Controls.Add(this.label11);
            this.tab2.Controls.Add(this.label10);
            this.tab2.Controls.Add(this.applyOtsu2BT);
            this.tab2.Location = new System.Drawing.Point(4, 22);
            this.tab2.Name = "tab2";
            this.tab2.Padding = new System.Windows.Forms.Padding(3);
            this.tab2.Size = new System.Drawing.Size(244, 288);
            this.tab2.TabIndex = 1;
            this.tab2.Text = "Automatic";
            this.tab2.UseVisualStyleBackColor = true;
            // 
            // rotsuTXT
            // 
            this.rotsuTXT.Location = new System.Drawing.Point(54, 205);
            this.rotsuTXT.Margin = new System.Windows.Forms.Padding(2);
            this.rotsuTXT.Name = "rotsuTXT";
            this.rotsuTXT.Size = new System.Drawing.Size(103, 20);
            this.rotsuTXT.TabIndex = 38;
            // 
            // applyRotsuBT
            // 
            this.applyRotsuBT.Location = new System.Drawing.Point(163, 230);
            this.applyRotsuBT.Name = "applyRotsuBT";
            this.applyRotsuBT.Size = new System.Drawing.Size(75, 23);
            this.applyRotsuBT.TabIndex = 37;
            this.applyRotsuBT.Text = "Apply";
            this.applyRotsuBT.UseVisualStyleBackColor = true;
            this.applyRotsuBT.Click += new System.EventHandler(this.applyRotsuBT_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 208);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "ROTSU";
            // 
            // applyCharBT
            // 
            this.applyCharBT.Location = new System.Drawing.Point(163, 165);
            this.applyCharBT.Name = "applyCharBT";
            this.applyCharBT.Size = new System.Drawing.Size(75, 23);
            this.applyCharBT.TabIndex = 35;
            this.applyCharBT.Text = "Apply";
            this.applyCharBT.UseVisualStyleBackColor = true;
            this.applyCharBT.Click += new System.EventHandler(this.applyCharBT_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 170);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Cheriet method";
            // 
            // applyEntrBT
            // 
            this.applyEntrBT.Location = new System.Drawing.Point(163, 124);
            this.applyEntrBT.Name = "applyEntrBT";
            this.applyEntrBT.Size = new System.Drawing.Size(75, 23);
            this.applyEntrBT.TabIndex = 33;
            this.applyEntrBT.Text = "Apply";
            this.applyEntrBT.UseVisualStyleBackColor = true;
            this.applyEntrBT.Click += new System.EventHandler(this.histEntrBT_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 11);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "OTSU thresholding method";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 129);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Histogram entropy";
            // 
            // applyOtsuBT
            // 
            this.applyOtsuBT.Location = new System.Drawing.Point(163, 6);
            this.applyOtsuBT.Name = "applyOtsuBT";
            this.applyOtsuBT.Size = new System.Drawing.Size(75, 23);
            this.applyOtsuBT.TabIndex = 21;
            this.applyOtsuBT.Text = "Apply";
            this.applyOtsuBT.UseVisualStyleBackColor = true;
            this.applyOtsuBT.Click += new System.EventHandler(this.ApplyOtsuBT_Click);
            // 
            // applyMinBT
            // 
            this.applyMinBT.Location = new System.Drawing.Point(163, 83);
            this.applyMinBT.Name = "applyMinBT";
            this.applyMinBT.Size = new System.Drawing.Size(75, 23);
            this.applyMinBT.TabIndex = 31;
            this.applyMinBT.Text = "Apply";
            this.applyMinBT.UseVisualStyleBackColor = true;
            this.applyMinBT.Click += new System.EventHandler(this.minErrBT_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 48);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "2D OTSU method";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 88);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Minimal error";
            // 
            // applyOtsu2BT
            // 
            this.applyOtsu2BT.Location = new System.Drawing.Point(163, 43);
            this.applyOtsu2BT.Name = "applyOtsu2BT";
            this.applyOtsu2BT.Size = new System.Drawing.Size(75, 23);
            this.applyOtsu2BT.TabIndex = 29;
            this.applyOtsu2BT.Text = "Apply";
            this.applyOtsu2BT.UseVisualStyleBackColor = true;
            this.applyOtsu2BT.Click += new System.EventHandler(this.ApplyAutoBT_Click);
            // 
            // angCHCK
            // 
            this.angCHCK.AutoSize = true;
            this.angCHCK.Location = new System.Drawing.Point(8, 34);
            this.angCHCK.Name = "angCHCK";
            this.angCHCK.Size = new System.Drawing.Size(96, 17);
            this.angCHCK.TabIndex = 27;
            this.angCHCK.Text = "High angularity";
            this.angCHCK.UseVisualStyleBackColor = true;
            // 
            // revertHistoBT
            // 
            this.revertHistoBT.Location = new System.Drawing.Point(100, 52);
            this.revertHistoBT.Name = "revertHistoBT";
            this.revertHistoBT.Size = new System.Drawing.Size(75, 23);
            this.revertHistoBT.TabIndex = 26;
            this.revertHistoBT.Text = "Revert";
            this.revertHistoBT.UseVisualStyleBackColor = true;
            this.revertHistoBT.Click += new System.EventHandler(this.RevertBT_Click);
            // 
            // methCMB
            // 
            this.methCMB.FormattingEnabled = true;
            this.methCMB.Location = new System.Drawing.Point(135, 13);
            this.methCMB.Name = "methCMB";
            this.methCMB.Size = new System.Drawing.Size(121, 21);
            this.methCMB.TabIndex = 25;
            // 
            // applyHistoBT
            // 
            this.applyHistoBT.Location = new System.Drawing.Point(181, 52);
            this.applyHistoBT.Name = "applyHistoBT";
            this.applyHistoBT.Size = new System.Drawing.Size(75, 23);
            this.applyHistoBT.TabIndex = 17;
            this.applyHistoBT.Text = "Apply";
            this.applyHistoBT.UseVisualStyleBackColor = true;
            this.applyHistoBT.Click += new System.EventHandler(this.EditHistBT_Click);
            // 
            // presCHCK
            // 
            this.presCHCK.AutoSize = true;
            this.presCHCK.Location = new System.Drawing.Point(8, 80);
            this.presCHCK.Name = "presCHCK";
            this.presCHCK.Size = new System.Drawing.Size(99, 17);
            this.presCHCK.TabIndex = 24;
            this.presCHCK.Text = "Perserve colors";
            this.presCHCK.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 16);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(122, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Edge evaluation method";
            // 
            // historyBT
            // 
            this.historyBT.Location = new System.Drawing.Point(410, 577);
            this.historyBT.Name = "historyBT";
            this.historyBT.Size = new System.Drawing.Size(75, 23);
            this.historyBT.TabIndex = 33;
            this.historyBT.Text = "Show";
            this.historyBT.UseVisualStyleBackColor = true;
            this.historyBT.Click += new System.EventHandler(this.historyBT_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 582);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Show in-between steps";
            // 
            // rmBgRotsuCHCK
            // 
            this.rmBgRotsuCHCK.AutoSize = true;
            this.rmBgRotsuCHCK.Location = new System.Drawing.Point(162, 208);
            this.rmBgRotsuCHCK.Name = "rmBgRotsuCHCK";
            this.rmBgRotsuCHCK.Size = new System.Drawing.Size(76, 17);
            this.rmBgRotsuCHCK.TabIndex = 35;
            this.rmBgRotsuCHCK.Text = "remove bg";
            this.rmBgRotsuCHCK.UseVisualStyleBackColor = true;
            // 
            // SegmentationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 635);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.historyBT);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.stateLBL);
            this.Controls.Add(this.outputNameBox);
            this.Controls.Add(this.ExportInfoBT);
            this.Controls.Add(this.AfterGroup);
            this.Controls.Add(this.BeforeGroup);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.ExportBT);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "SegmentationView";
            this.Text = "Image segmentation II";
            this.Load += new System.EventHandler(this.SteganographyView_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbNameERR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputERR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beforePic)).EndInit();
            this.BeforeGroup.ResumeLayout(false);
            this.beforePanel.ResumeLayout(false);
            this.AfterGroup.ResumeLayout(false);
            this.afterPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.afterPic)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem confOutputMN;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label nameUpTXT;
        private System.Windows.Forms.Label sizeUpTXT;
        private System.Windows.Forms.Label sizeUpLBL;
        private System.Windows.Forms.Label nameUpLBL;
        private System.Windows.Forms.Label pathLBL;
        private System.Windows.Forms.Label extractLBL;
        private System.Windows.Forms.ErrorProvider dbNameERR;
        private System.Windows.Forms.ErrorProvider inputERR;
        private System.Windows.Forms.ToolStripMenuItem loadFileToolStripMenuItem;
        private System.Windows.Forms.GroupBox AfterGroup;
        private System.Windows.Forms.PictureBox afterPic;
        private System.Windows.Forms.GroupBox BeforeGroup;
        private System.Windows.Forms.PictureBox beforePic;
        private System.Windows.Forms.Button ExportBT;
        private System.Windows.Forms.Button histogramBT;
        private System.Windows.Forms.Panel beforePanel;
        private System.Windows.Forms.Panel afterPanel;
        private System.Windows.Forms.Button deleteBT;
        private System.Windows.Forms.Label pathTXT;
        private System.Windows.Forms.Label stateLBL;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoOneStepToolStripMenuItem;
        private System.Windows.Forms.ComboBox edgeEvalCMB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox outputNameBox;
        private System.Windows.Forms.Button ExportInfoBT;
        private System.Windows.Forms.Button matrixBT;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox manTXT;
        private System.Windows.Forms.Button applyManBT;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox probTXT;
        private System.Windows.Forms.Button applyPercBT;
        private System.Windows.Forms.TabPage tab2;
        private System.Windows.Forms.Button applyEntrBT;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button applyOtsuBT;
        private System.Windows.Forms.Button applyMinBT;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button applyOtsu2BT;
        private System.Windows.Forms.CheckBox angCHCK;
        private System.Windows.Forms.Button revertHistoBT;
        private System.Windows.Forms.ComboBox methCMB;
        private System.Windows.Forms.Button applyHistoBT;
        private System.Windows.Forms.CheckBox presCHCK;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button applyCharBT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button sauvolaBT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox sauvolaTXT;
        private System.Windows.Forms.ComboBox sauvolaCMB;
        private System.Windows.Forms.Button applyRotsuBT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rotsuTXT;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button historyBT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox rmBgRotsuCHCK;
    }
}

