﻿using Segmentation.Model.Objects;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Segmentation.View
{
    /// <summary>
    /// Class representing the behaviour of histogram window
    /// </summary>
    public partial class HistogramView : Form
    {
        /// <summary> Image of displayed histogram </summary>
        DataFile displayed;

        /// <summary> Title of graph </summary>
        string graphtitle;
        
        /// <summary> Label of x axis </summary>
        string xAxis;
        /// <summary> Label of y axis </summary>
        string yAxis;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="title"> Title of window </param>
        /// <param name="graphtitle"> Title of the graph </param>
        /// <param name="yVals"> Y values </param>
        /// <param name="label"> Label </param>
        /// <param name="xAxis"> X axis label </param>
        /// <param name="yAxis"> Y axis label </param> 
        public HistogramView(string title, string graphtitle, DataFile disp, string label, string xAxis, string yAxis)
        {
            InitializeComponent();
            
            this.displayed = disp;
            this.graphtitle = graphtitle;
            this.xAxis = xAxis;
            this.yAxis = yAxis;
            this.Text = title;
            
            double[] yVals = disp.Histogram;
            SetUpGraph(yVals, label, xAxis, yAxis);
        }

        /// <summary>
        /// Set up graph
        /// </summary>
        /// <param name="yVals"> Y values </param>
        /// <param name="label"> Label </param>
        /// <param name="xAxis"> Label of x axis </param>
        /// <param name="yAxis"> Label of y axis </param>
        private void SetUpGraph(double[] yVals, string label, string xAxis, string yAxis)
        {
            Series series1 = new Series(label);
            series1.Points.DataBindY(yVals);
            series1.ChartType = SeriesChartType.Column;

            // add each series to the chart
            histogram.Series.Clear();
            histogram.Series.Add(series1);

            // additional styling
            histogram.ResetAutoValues();
            histogram.Titles.Clear();
            histogram.Titles.Add(graphtitle);
            histogram.ChartAreas[0].AxisX.Title = xAxis;
            histogram.ChartAreas[0].AxisY.Title = yAxis;
            histogram.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            histogram.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
        }

        // --------------------------------- BUTTONS -------------------------------

        /// <summary>
        /// Red button clicked
        /// </summary>
        private void redBT_Click(object sender, System.EventArgs e)
        {
            SetUpGraph(displayed.HistogramRGB[0], "Red brightness", xAxis, yAxis);
        }

        /// <summary>
        /// Green button clicked
        /// </summary>
        private void greenBT_Click(object sender, System.EventArgs e)
        {
            SetUpGraph(displayed.HistogramRGB[1], "Green brightness", xAxis, yAxis);
        }

        /// <summary>
        /// Blue button clicked
        /// </summary>
        private void blueBT_Click(object sender, System.EventArgs e)
        {
            SetUpGraph(displayed.HistogramRGB[2], "Blue brightness", xAxis, yAxis);
        }

        /// <summary>
        /// Original button clicked
        /// </summary>
        private void origBT_Click(object sender, System.EventArgs e)
        {
            SetUpGraph(displayed.Histogram, "Original brightness", xAxis, yAxis);
        }

        private void relativeBT_Click(object sender, System.EventArgs e)
        {
            SetUpGraph(displayed.HistogramRGB[3], "Relative histogram", xAxis, "% of pixels (0=0%, 1=100%)");
        }
    }
}
