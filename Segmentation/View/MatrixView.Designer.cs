﻿
namespace Segmentation.View
{
    partial class MatrixView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MatrixView));
            this.matrixCHRT = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.matrixCHRT)).BeginInit();
            this.SuspendLayout();
            // 
            // matrixCHRT
            // 
            chartArea1.Name = "ChartArea1";
            this.matrixCHRT.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.matrixCHRT.Legends.Add(legend1);
            this.matrixCHRT.Location = new System.Drawing.Point(12, 12);
            this.matrixCHRT.Name = "matrixCHRT";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.matrixCHRT.Series.Add(series1);
            this.matrixCHRT.Size = new System.Drawing.Size(636, 396);
            this.matrixCHRT.TabIndex = 0;
            this.matrixCHRT.Text = "chart1";
            // 
            // MatrixView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 420);
            this.Controls.Add(this.matrixCHRT);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MatrixView";
            this.Text = "MatrixView";
            ((System.ComponentModel.ISupportInitialize)(this.matrixCHRT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart matrixCHRT;
    }
}