﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Segmentation.View
{
    public partial class MatrixView : Form
    {
        public MatrixView(string title, string graphtitle, double[] xVal, double[] yVal)
        {
            InitializeComponent();
            this.Text = title;
            SetUpGraph(graphtitle, xVal, yVal);
        }

        private void SetUpGraph(string title, double[] xVal, double[] yVal)
        {
            Series series1 = new Series("Non zero values");
            series1.Points.DataBindXY(xVal, yVal);
            series1.ChartType = SeriesChartType.Point;
            series1.MarkerStyle = MarkerStyle.Circle;

            // add each series to the chart
            matrixCHRT.Series.Clear();
            matrixCHRT.Series.Add(series1);

            // additional styling
            matrixCHRT.ResetAutoValues();
            matrixCHRT.Titles.Clear();
            matrixCHRT.Titles.Add(title);
            matrixCHRT.ChartAreas[0].AxisX.Title = "Brightness value";
            matrixCHRT.ChartAreas[0].AxisY.Title = "Operator value";
            matrixCHRT.ChartAreas[0].AxisY.IsStartedFromZero = false;
            matrixCHRT.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            matrixCHRT.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
        }
    }
}
