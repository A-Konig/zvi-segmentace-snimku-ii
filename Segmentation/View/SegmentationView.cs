﻿using Segmentation.Model.Objects;
using Segmentation.Presenter;
using Segmentation.View;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Segmentation
{

    /// <summary>
    /// Main window view 
    /// </summary>
    public partial class SegmentationView : Form
    {
        string[] edgeDetectionMethods = new string[] { "Laplace", "Roberts-Cross", "DIF", "Sobel", "Robinson" };
        bool otsuDisabled;

        string[] sauvolaThresholdMethods = new string[] { "Sauvola", "Niblack", "Wolf" };


        /// <summary> Presenter </summary>
        public SteganographyPresenter Presenter { private get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SegmentationView()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;

            SetUpComponents();
        }

        /// <summary>
        /// Set up image components
        /// </summary>
        private void SetUpComponents()
        {
            methCMB.DataSource = edgeDetectionMethods;
            methCMB.DropDownStyle = ComboBoxStyle.DropDownList;

            sauvolaCMB.DataSource = sauvolaThresholdMethods;
            sauvolaCMB.DropDownStyle = ComboBoxStyle.DropDownList;

            // before pic scrollbar
            beforePic.SizeMode = PictureBoxSizeMode.AutoSize;
            beforePanel.AutoScroll = true;
            beforePanel.Controls.Add(beforePic);

            // after pic scrollbar
            afterPic.SizeMode = PictureBoxSizeMode.AutoSize;
            afterPanel.AutoScroll = true;
            afterPanel.Controls.Add(afterPic);

        }

        /// <summary>
        /// Open action result dialog
        /// </summary>
        /// <param name="title"> Title of dialog </param>
        /// <param name="succ"> Success message </param>
        /// <param name="unsucc"> Fail message </param>
        /// <param name="result"> Result of action </param>
        private void OpenResultDialog(string title, string succ, string unsucc, bool result)
        {
            if (this.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { OpenResultDialog(title, succ, unsucc, result); }));
            else
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                if (result)
                    MessageBox.Show(succ, title, buttons, MessageBoxIcon.Information);
                else
                    MessageBox.Show(unsucc, title, buttons, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Open question dialog
        /// </summary>
        /// <param name="title"> Title </param>
        /// <param name="text"> Text </param>
        /// <returns> True if OK pressed, otherwise false </returns>
        private bool OpenQuestionDialog(string title, string text)
        {
            if (this.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { OpenQuestionDialog(title, text); }));
            else
            {
                DialogResult res = MessageBox.Show(text, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (res == DialogResult.OK)
                    return true;
                if (res == DialogResult.Cancel)
                    return false;
            }

            return false;
        }

        /// <summary>
        /// On load
        /// </summary>
        private void SteganographyView_Load(object sender, EventArgs e) { }

        ////// ------- MENU OPTION -------

        /// <summary>
        /// Configure input path
        /// </summary>
        private void confOutputMN_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true,
                SelectedPath = Config.OutputPath
            };

            if (fd.ShowDialog() == DialogResult.OK)
            {
                Config.OutputPath = fd.SelectedPath;
            }
        }

        /// <summary>
        /// Load new image to edit
        /// </summary>
        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                InitialDirectory = Config.InputPath,
                Title = "Select input image file",
                CheckFileExists = true,
                CheckPathExists = true,
                Filter = "Images (*.BMP;*.PNG;*.JPG;*.JPEG)|*.BMP;*.PNG;*.JPG;*.JPEG",
                RestoreDirectory = true,
                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {

                stateLBL.Text = "Loading file...";

                Thread t = new Thread(() =>
                {
                    DataFile dt = new DataFile(dialog.FileName);

                    if (dt.Image != null)
                    {
                        SetLabelText(pathTXT, dialog.FileName);
                        SetLabelText(nameUpTXT, dt.FullName);
                        SetLabelText(sizeUpTXT, dt.Length);
                        SetImage(beforePic, dt);
                        SetImage(afterPic, dt);
                        TogglePauseActivities(true);
                        SetTextField(outputNameBox, "");
                        SetLabelText(stateLBL, "");
                    }

                    // save image for further processing
                    Presenter.SetCurrentDataFile(dt);
                });
                t.Start();
            }
        }

        /// <summary>
        /// Undo
        /// </summary>
        private void undoOneStepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Presenter.Undo();
            DataFile dt = Presenter.GetCurrentEditedFile();
            if (dt != null)
                afterPic.Image = dt.Image;
        }

        // ------ INVOKES -------

        /// <summary>
        /// Set image view
        /// </summary>
        /// <param name="img"> Image box </param>
        /// <param name="bmp"> Image </param>
        private void SetImage(PictureBox img, DataFile bmp)
        {
            if (img.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { SetImage(img, bmp); }));
            else
                img.Image = new Bitmap(bmp.Image);
        }

        /// <summary>
        /// Set label text
        /// </summary>
        /// <param name="lbl"> Label </param>
        /// <param name="txt"> Text </param>
        private void SetLabelText(Label lbl, string txt)
        {
            if (lbl.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { SetLabelText(lbl, txt); }));
            else
                lbl.Text = txt;
        }

        /// <summary>
        /// Set text box text
        /// </summary>
        /// <param name="box"> Text box </param>
        /// <param name="txt"> Text </param>
        private void SetTextField(TextBox box, string txt)
        {
            if (box.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { SetTextField(box, txt); }));
            else
                box.Text = txt;
        }

        /// <summary>
        /// Set button enabled
        /// </summary>
        /// <param name="btn"> Button </param>
        /// <param name="active"> True if enabled, false if  not </param>
        private void ToggleButtonActive(Button btn, bool active)
        {
            if (btn.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { ToggleButtonActive(btn, active); }));
            else
                btn.Enabled = active;
        }


        /// <summary>
        /// Pause/unpause buttons
        /// </summary>
        /// <param name="active"> True if button should be active, false if not </param>
        private void TogglePauseActivities(bool active)
        {
            if (deleteBT.InvokeRequired || revertHistoBT.InvokeRequired    || applyHistoBT.InvokeRequired  ||
                applyManBT.InvokeRequired  || applyPercBT.InvokeRequired || applyOtsuBT.InvokeRequired ||
                applyOtsu2BT.InvokeRequired || matrixBT.InvokeRequired || histogramBT.InvokeRequired ||
                applyCharBT.InvokeRequired || applyEntrBT.InvokeRequired || applyMinBT.InvokeRequired ||
                applyRotsuBT.InvokeRequired || sauvolaBT.InvokeRequired)
                Invoke(new MethodInvoker(delegate () { TogglePauseActivities(active); }));
            else
            {
                deleteBT.Enabled = active;
                revertHistoBT.Enabled = active;
                applyHistoBT.Enabled = active;
                applyManBT.Enabled = active;
                applyPercBT.Enabled = active;
                applyOtsuBT.Enabled = active;
                applyOtsu2BT.Enabled = active;
                matrixBT.Enabled = active;
                histogramBT.Enabled = active;
                applyCharBT.Enabled = active;
                applyEntrBT.Enabled = active;
                applyMinBT.Enabled = active;
                applyRotsuBT.Enabled = active;
                sauvolaBT.Enabled = active;
            }
        }

        // ----- BUTTON CLICKS -----

        /// <summary>
        /// Delete input file from program
        /// </summary>
        private void deleteBT_Click_1(object sender, EventArgs e)
        {
            // reset
            pathTXT.Text = "< N/A >";
            nameUpTXT.Text = "< N/A >";
            sizeUpTXT.Text = "0 kB";

            bool res = OpenQuestionDialog("Close file", "Are you sure you want to close current file? All changes will be lost");

            if (res)
            {
                beforePic.Image = null;
                afterPic.Image = null;
                Presenter.SetCurrentDataFile(null);
            }
        }

        /// <summary>
        /// Apply user selected threshold
        /// </summary>
        private void ApplyBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Applying manually selected threshold...";
            TogglePauseActivities(false);
            Thread t = new Thread(() =>
            {
                bool res = Presenter.SetThreshold(manTXT.Text, presCHCK.Checked);

                // show picture
                if (res != false)
                {
                    DataFile df = Presenter.GetCurrentEditedFile();
                    SetImage(afterPic, df);
                }

                OpenResultDialog("Setting threshold",
                                 "Modifying input file finished",
                                 "Incorrect input while setting threshold",
                                 res == true);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);

            });
            t.Start();
        }

        /// <summary>
        /// Save edited image
        /// </summary>
        private void ExportBT_Click(object sender, EventArgs e)
        {
            string name = outputNameBox.Text;
            stateLBL.Text = "Exporting image...";

            Thread t = new Thread(() =>
            {
                string res = Presenter.SaveImage(name);
                OpenResultDialog("Exporting image", "Files saved into " + res, "Exporting unsuccessfull", res != null);
                SetLabelText(stateLBL, "");
            });
            t.Start();
        }

        /// <summary>
        /// Save information about input file
        /// </summary>
        private void ExportInfoBT_Click(object sender, EventArgs e)
        {
            string name = outputNameBox.Text;
            stateLBL.Text = "Exporting information...";

            Thread t = new Thread(() =>
            {
                string res = Presenter.SaveTextInfo(name);
                OpenResultDialog("Exporting text information", "Files saved into " + res, "Exporting unsuccessfull", res != null);
                SetLabelText(stateLBL, "");
            });
            t.Start();
        }

        /// <summary>
        /// Show histogram window
        /// </summary>
        private void histogramBT_Click(object sender, EventArgs e)
        {
            DataFile d = Presenter.GetCurrentDataFile();
            if (d == null)
                return;

            HistogramView histogram = new HistogramView(d.FullName + " histogram ", d.HistogramType, d.DeepCopy(), "General brightness", "Brightness level", "Amount of pixels");
            histogram.Show();
        }

        /// <summary>
        /// Edit histogram
        /// </summary>
        private void EditHistBT_Click(object sender, EventArgs e)
        {
            int index = methCMB.SelectedIndex;
            bool edgeCase = angCHCK.Checked;

            stateLBL.Text = $"Editing histogram using {edgeDetectionMethods[index]}...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                bool res = Presenter.EditHistogram(index, edgeCase);
                OpenResultDialog($"Changing histogram using {edgeDetectionMethods[index]}", "Histogram has been edited", "Operation coudln't be done successfully", res);

                TogglePauseActivities(true);
                if (!res)
                    otsuDisabled = false;
                else
                    otsuDisabled = true;

                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);

                SetLabelText(stateLBL, "");
            });
            t.Start();
        }

        /// <summary>
        /// Revert edits to histogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RevertBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Reversing edits to histogram...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                bool res = Presenter.EditHistogram(-1, false);
                OpenResultDialog("Reverting changes to histogram", "Original histogram has been restored", "Operation coudln't be done successfully", res);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);

                ToggleButtonActive(applyOtsuBT, true);
                otsuDisabled = false;
            });
            t.Start();
        }

        /// <summary>
        /// Apply thresholding using probability
        /// </summary>
        private void ApplyProbBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing based on probability of object...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                string res = Presenter.SetProbability(probTXT.Text, presCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing based on probability of object", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);
            });
            t.Start();
        }

        /// <summary>
        /// Apply thresholding using OTSU
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyOtsuBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Computing OTSU method...";
            TogglePauseActivities(false);
            bool keep = presCHCK.Checked;

            Thread t = new Thread(() =>
            {
                string res = Presenter.OTSU(keep);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("OTSU method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);

            });
            t.Start();
        }

        /// <summary>
        /// Show angularity matrix of current data file
        /// </summary>
        private void matrixBT_Click(object sender, EventArgs e)
        {
            double[][] data = Presenter.GetCurrentMatrixData();
            DataFile df = Presenter.GetCurrentDataFile();

            if (data != null)
            {
                MatrixView matrix = new MatrixView(df.FullName + " angularity matrix", df.HistogramType, data[0], data[1]);
                matrix.Show();
            }
        }

        /// <summary>
        /// Compute new output using 2D otsu
        /// </summary>
        private void ApplyAutoBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Computing 2D OTSU method...";
            TogglePauseActivities(false);
            bool keep = presCHCK.Checked;

            Thread t = new Thread(() =>
            {
                string res = Presenter.OTSU2D(keep);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("2D OTSU method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);

            });
            t.Start();
        }

        private void oTSUToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Computing OTSU method...";
            TogglePauseActivities(false);
            bool keep = presCHCK.Checked;

            Thread t = new Thread(() =>
            {
                string res = Presenter.OTSU(keep);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("OTSU method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);

            });
            t.Start();
        }

        private void dOTSUToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Computing 2D OTSU method...";
            TogglePauseActivities(false);
            bool keep = presCHCK.Checked;

            Thread t = new Thread(() =>
            {
                string res = Presenter.OTSU2D(keep);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("2D OTSU method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
                //ToggleButtonActive(ApplyOtsuBT, !otsuDisabled);

            });
            t.Start();
        }

        private void minimalErrorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing based on minimum error...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                string res = Presenter.MinimumError(presCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing based on minimum error", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
            });
            t.Start();
        }

        private void minErrBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing based on minimum error...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                string res = Presenter.MinimumError(presCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing based on minimum error", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
            });
            t.Start();
        }

        private void histEntrBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing based on histogram entropy...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                // TODO input of step
                string res = Presenter.HistogramEntropy(0.1, presCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing based on histogram entropy", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
            });
            t.Start();
        }

        private void sauvolaBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing using Niblack method...";
            TogglePauseActivities(false);
            int method = sauvolaCMB.SelectedIndex;
            string args = sauvolaTXT.Text;

            Thread t = new Thread(() =>
            {
                string res = Presenter.Sauvola(args, method, presCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing using Niblack method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
            });
            t.Start();
        }

        private void applyCharBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing using Cheriet method...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                // TODO input for parameter?
                string res = Presenter.Cheriet("95", presCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing using Cheriet method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
            });
            t.Start();
        }

        private void applyRotsuBT_Click(object sender, EventArgs e)
        {
            stateLBL.Text = "Dividing using Recursive Otsu method...";
            TogglePauseActivities(false);

            Thread t = new Thread(() =>
            {
                string res = Presenter.ROTSU(rotsuTXT.Text, presCHCK.Checked, rmBgRotsuCHCK.Checked);
                DataFile dt = Presenter.GetCurrentEditedFile();

                SetTextField(manTXT, "" + res);
                if (dt != null)
                    SetImage(afterPic, dt);

                OpenResultDialog("Dividing using Recursive Otsu method", "Output created", "Output couldn't be created", dt != null && res.Length != 0);
                SetLabelText(stateLBL, "");
                TogglePauseActivities(true);
            });
            t.Start();
        }


        private void historyBT_Click_1(object sender, EventArgs e)
        {
            DataFile d = Presenter.GetCurrentEditedFile();
            if (d == null)
                return;

            HistoryView history = new HistoryView(d.PreProcessFiles);
            history.Show();
        }
    }
}