﻿using Segmentation.Model.Objects;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Segmentation.View
{
    /// <summary>
    /// Class handling the display of in-between steps of latest thresholding of the input image
    /// </summary>
    public partial class HistoryView : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="files"> Files to display </param>
        public HistoryView(List<DataFile> files)
        {
            InitializeComponent();

            if (files != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    TabPage t = new TabPage("In between step" + (i+1));

                    PictureBox pic = new PictureBox();
                    // pic.Dock = DockStyle.Fill;
                    pic.SizeMode = PictureBoxSizeMode.AutoSize;

                    Panel p = new Panel();
                    p.Dock = DockStyle.Fill;
                    p.Controls.Add(pic);
                    p.AutoScroll = true;

                    t.Controls.Add(p);

                    pic.Image = files[i].Image;
                    imagesTB.Controls.Add(t);

                }
            }
        }
    }
}
