﻿
namespace Segmentation.View
{
    partial class HistogramView
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistogramView));
            this.histogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.redBT = new System.Windows.Forms.Button();
            this.greenBT = new System.Windows.Forms.Button();
            this.blueBT = new System.Windows.Forms.Button();
            this.origBT = new System.Windows.Forms.Button();
            this.relativeBT = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.histogram)).BeginInit();
            this.SuspendLayout();
            // 
            // histogram
            // 
            chartArea1.Name = "ChartArea1";
            this.histogram.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.histogram.Legends.Add(legend1);
            this.histogram.Location = new System.Drawing.Point(12, 12);
            this.histogram.Name = "histogram";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.histogram.Series.Add(series1);
            this.histogram.Size = new System.Drawing.Size(637, 365);
            this.histogram.TabIndex = 7;
            this.histogram.Text = "chart1";
            // 
            // redBT
            // 
            this.redBT.Location = new System.Drawing.Point(225, 385);
            this.redBT.Name = "redBT";
            this.redBT.Size = new System.Drawing.Size(75, 23);
            this.redBT.TabIndex = 8;
            this.redBT.Text = "Red";
            this.redBT.UseVisualStyleBackColor = true;
            this.redBT.Click += new System.EventHandler(this.redBT_Click);
            // 
            // greenBT
            // 
            this.greenBT.Location = new System.Drawing.Point(306, 385);
            this.greenBT.Name = "greenBT";
            this.greenBT.Size = new System.Drawing.Size(75, 23);
            this.greenBT.TabIndex = 9;
            this.greenBT.Text = "Green";
            this.greenBT.UseVisualStyleBackColor = true;
            this.greenBT.Click += new System.EventHandler(this.greenBT_Click);
            // 
            // blueBT
            // 
            this.blueBT.Location = new System.Drawing.Point(387, 385);
            this.blueBT.Name = "blueBT";
            this.blueBT.Size = new System.Drawing.Size(75, 23);
            this.blueBT.TabIndex = 10;
            this.blueBT.Text = "Blue";
            this.blueBT.UseVisualStyleBackColor = true;
            this.blueBT.Click += new System.EventHandler(this.blueBT_Click);
            // 
            // origBT
            // 
            this.origBT.Location = new System.Drawing.Point(144, 385);
            this.origBT.Name = "origBT";
            this.origBT.Size = new System.Drawing.Size(75, 23);
            this.origBT.TabIndex = 11;
            this.origBT.Text = "General";
            this.origBT.UseVisualStyleBackColor = true;
            this.origBT.Click += new System.EventHandler(this.origBT_Click);
            // 
            // relativeBT
            // 
            this.relativeBT.Location = new System.Drawing.Point(468, 385);
            this.relativeBT.Name = "relativeBT";
            this.relativeBT.Size = new System.Drawing.Size(75, 23);
            this.relativeBT.TabIndex = 12;
            this.relativeBT.Text = "Relative";
            this.relativeBT.UseVisualStyleBackColor = true;
            this.relativeBT.Click += new System.EventHandler(this.relativeBT_Click);
            // 
            // HistogramView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 418);
            this.Controls.Add(this.relativeBT);
            this.Controls.Add(this.origBT);
            this.Controls.Add(this.blueBT);
            this.Controls.Add(this.greenBT);
            this.Controls.Add(this.redBT);
            this.Controls.Add(this.histogram);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HistogramView";
            this.Text = "Histogram";
            ((System.ComponentModel.ISupportInitialize)(this.histogram)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart histogram;
        private System.Windows.Forms.Button redBT;
        private System.Windows.Forms.Button greenBT;
        private System.Windows.Forms.Button blueBT;
        private System.Windows.Forms.Button origBT;
        private System.Windows.Forms.Button relativeBT;
    }
}